

export class Configurations {

    public static readonly MDR_API_ADDRESS: string = 'https://aitapp-mdr.herokuapp.com';

    public static readonly MDV_API_ADDRESS: string = 'https://aitapp-mdv.herokuapp.com';

    public static readonly PLAN_API_ADDRESS: string = 'https://aitapp-planeamento.herokuapp.com';

    public static readonly ASSETS_LOCATION: string = 'assets/visualization3d/';

    public static readonly DEFAULT_MODEL: string = 'assets/visualization3d/default.glb';

    public static readonly CAR_MODEL: string = 'assets/visualization3d/car.glb';

    public static readonly DATA_ADMINISTRATOR: string = 'DATA_ADMINISTRATOR';

    public static readonly MANAGER: string = 'MANAGER';

    public static readonly CLIENT: string = 'CLIENT';

}