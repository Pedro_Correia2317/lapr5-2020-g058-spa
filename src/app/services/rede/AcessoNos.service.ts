
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PedidoCriarNoDTO } from 'src/app/models/rede/no/PedidoNoDTO';
import { TipoNoDTO } from 'src/app/models/rede/no/TipoNoDTO';
import { DetalhesOperacaoDTO } from 'src/app/models/rede/pedidos/DetalhesOperacaoDTO';
import { NoDTO } from '../../models/rede/no/NoDTO';
import { DetalhesPesquisaDTO } from '../../models/rede/pedidos/DetalhesPesquisaDTO';
import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AcessoNosService {

  private enderecoHttp: string = `${Configurations.MDR_API_ADDRESS}/nos`;

  private enderecoHttpTiposNos: string = `${Configurations.MDR_API_ADDRESS}/tiposNos`;

  constructor(private http: HttpClient) { }

  procurarNos(pagina: number, tamanho: number, sort?: string, nome?: string, desc?: boolean, filtro?: string): Observable<DetalhesPesquisaDTO<NoDTO>> {
    const aux: string = this.escolherSort(sort);
    const filtroAux: string = this.escolherFiltro(filtro);
    const nomeAux: string = nome? nome : '';
    const order: string = desc? 'desc' : 'asc';
    return this.http.get<DetalhesPesquisaDTO<NoDTO>>(this.enderecoHttp, { 
        observe: 'body',
        responseType: 'json',
        params: {
          page: '' + pagina,
          size: '' + tamanho,
          sort: aux,
          nome: nomeAux,
          order: order,
          filter: filtroAux
        }
      });
  }

  enviarNo(pedido: PedidoCriarNoDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }

  procurarTiposNos(): Observable<DetalhesPesquisaDTO<TipoNoDTO>> {
    return this.http.get<DetalhesPesquisaDTO<TipoNoDTO>>(this.enderecoHttpTiposNos, {
      observe: 'body',
      responseType: 'json'
    });
  }

  private escolherSort(sort?: string): string {
    if(sort != null && sort != ''){
      switch (sort){
        case 'DATA_DE_ADICAO':
          return 'date';
        case 'NOME':
          return 'nome';
        case 'TIPO_NO':            
          return 'tipoNo';
        case 'ABREVIATURA':
          return 'abreviatura';
      }
    }
    return '';
  }

  private escolherFiltro(filtro?: string): string {
    if(filtro != null && filtro != ''){
      switch (filtro){
        case 'NENHUM':
          return 'none';
        case 'NO_PARAGEM':
          return 'stop_points';
        case 'NO_ESTACAO_RECOLHA':            
          return 'collection_station';
        case 'NO_PONTO_RENDICAO':
          return 'rendition_points';
      }
    }
    return '';
  }
}
