
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/rede/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarTipoViaturaDTO } from 'src/app/models/rede/tipoViatura/PedidoCriarTipoViaturaDTO';
import { TipoCombustivelDTO } from 'src/app/models/rede/tipoViatura/TipoCombustivelDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { TipoViaturaDTO } from 'src/app/models/rede/tipoViatura/TipoViaturaDTO';
import { TipoViatura } from 'src/app/models/rede/tipoViatura/TipoViatura';
import { TipoCombustivel } from 'src/app/models/rede/tipoViatura/TipoCombustivel';

@Injectable({
  providedIn: 'root'
})
export class AcessoTiposViaturasService {

  private enderecoHttp: string = `${Configurations.MDR_API_ADDRESS}/tiposViaturas`;

  private enderecoHttpTiposCombustiveis: string = `${Configurations.MDR_API_ADDRESS}/tiposCombustiveis`;

  private endTipoViatura: string = `${Configurations.MDR_API_ADDRESS}/tipoViatura/`;

  constructor(private http: HttpClient) { }

  procurarTiposViaturas(pagina: number, tamanho: number, sort?: string, desc?: string, isDesc?: boolean, filtro?: string): Observable<DetalhesPesquisaDTO<TipoViaturaDTO>> {
    const aux: string = this.escolherSort(sort);
    const filtroAux: string = this.escolherFiltro(filtro);
    const descAux: string = desc? desc : '';
    const order: string = isDesc? 'desc' : 'asc';
    return this.http.get<DetalhesPesquisaDTO<TipoViaturaDTO>>(this.enderecoHttp, {
      observe: 'body',
      responseType: 'json',
      params: {
        page: '' + pagina,
        size: '' + tamanho,
        sort: aux,
        descricao: descAux,
        order: order,
        filter: filtroAux
      }
    });
  }

  enviarTipoViatura(pedido: PedidoCriarTipoViaturaDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }

  procurarTiposCombustiveis(): Observable<DetalhesPesquisaDTO<TipoCombustivelDTO>> {
    return this.http.get<DetalhesPesquisaDTO<TipoCombustivelDTO>>(this.enderecoHttpTiposCombustiveis, {
      observe: 'body',
      responseType: 'json'
    });
  }

  procurarTipoViatura(codigo: string): Observable<TipoViatura> {
    return new Observable<TipoViatura>(observer => {
        this.http.get<TipoViaturaDTO>(this.endTipoViatura + codigo, {
          observe: 'body',
          responseType: 'json'
        }).subscribe((results: TipoViaturaDTO) => {
            const tv = new TipoViatura();
            tv.codigo = results.codigo;
            tv.autonomia = results.autonomia;
            tv.consumo = results.consumo;
            tv.custo = results.custo;
            tv.descricao = results.descricao;
            tv.emissoes = results.emissoes;
            tv.velocidadeMedia = results.velocidadeMedia;
            const tc: TipoCombustivel = new TipoCombustivel();
            tc.descricao = results.tipoCombustivel.descricao;
            tc.unidade = results.tipoCombustivel.unidade;
            tv.tipoCombustivel = tc;
            observer.next(tv);
        }, error => observer.error(error));
    });
  }

  private escolherSort(sort?: string): string {
    if(sort != null && sort != ''){
      switch (sort){
        case 'DATA_DE_ADICAO':
          return 'date';
        case 'CODIGO':
          return 'codigo';
        case 'DESCRICAO':
          return 'descricao';
        case 'AUTONOMIA':            
          return 'autonomia';
        case 'VELOCIDADE':
          return 'velocidadeMedia';
        case 'CUSTO':
          return 'custo';
        case 'CONSUMO':
          return 'consumo';
        case 'EMISSOES':            
          return 'emissoes';
        case 'COMBUSTIVEL':
          return 'tipoCombustivel';
      }
    }
    return '';
  }

  private escolherFiltro(filtro?: string): string {
    if(filtro != null && filtro != ''){
      switch (filtro){
        case 'NENHUM':
          return 'none';
        case 'DIESEL':
          return 'diesel';
        case 'GASOLINA':            
          return 'gasolina';
        case 'GPL':
          return 'gpl';
        case 'HIDROGENIO':
          return 'hidrogenio';
        case 'ELECTRICO':            
          return 'electrico';
      }
    }
    return '';
  }

}