
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/rede/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarTipoTripulanteDTO } from 'src/app/models/rede/tiposTripulantes/PedidoCriarTipoTripulanteDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { TipoTripulanteDTO } from 'src/app/models/rede/tiposTripulantes/TipoTripulanteDTO';
import { TipoTripulante } from 'src/app/models/rede/tiposTripulantes/TipoTripulante';

@Injectable({
  providedIn: 'root'
})
export class AcessoTiposTripulantesService {

  private enderecoHttp: string = `${Configurations.MDR_API_ADDRESS}/tiposTripulantes`;

  private endTipoTripulante: string = `${Configurations.MDR_API_ADDRESS}/tipoTripulante/`;

  constructor(private http: HttpClient) { }

  procurarTiposTripulantes(pagina: number, tamanho: number, sort?: string, desc?: string, isDesc?: boolean): Observable<DetalhesPesquisaDTO<TipoTripulanteDTO>> {
    const aux: string = this.escolherSort(sort);
    const descAux: string = desc? desc : '';
    const order: string = isDesc? 'desc' : 'asc';
    return this.http.get<DetalhesPesquisaDTO<TipoTripulanteDTO>>(this.enderecoHttp, {
      observe: 'body',
      responseType: 'json',
      params: {
        page: '' + pagina,
        size: '' + tamanho,
        sort: aux,
        descricao: descAux,
        order: order
      }
    });
  }

  enviarTipoTripulante(pedido: PedidoCriarTipoTripulanteDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }
  
  procurarTipoTripulante(codigo: string): Observable<TipoTripulante> {
    return new Observable<TipoTripulante>(observer => {
        this.http.get<TipoTripulanteDTO>(this.endTipoTripulante + codigo, {
          observe: 'body',
          responseType: 'json'
        }).subscribe((results: TipoTripulanteDTO) => {
            const tt = new TipoTripulante();
            tt.codigo = results.codigo;
            tt.descricao = results.descricao;
            observer.next(tt);
        }, error => observer.error(error));
    });
  }
  
  private escolherSort(sort?: string): string {
    if(sort != null && sort != ''){
      switch (sort){
        case 'DATA_DE_ADICAO':
          return 'date';
        case 'CODIGO':
          return 'codigo';
        case 'DESCRICAO':
          return 'descricao';
      }
    }
    return '';
  }
}