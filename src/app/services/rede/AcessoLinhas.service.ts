
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/rede/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarLinhaDTO } from 'src/app/models/rede/linhas/PedidoCriarLinhaDTO';
import { LinhaDTO } from 'src/app/models/rede/linhas/LinhaDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { PercursoDTO } from 'src/app/models/rede/percursos/PercursoDTO';
import { Linha } from 'src/app/models/rede/linhas/Linha';
import { LinhaCompletaDTO } from 'src/app/models/rede/linhas/LinhaCompletaDTO';

@Injectable({
  providedIn: 'root'
})
export class AcessoLinhasService {

  private enderecoHttp: string = `${Configurations.MDR_API_ADDRESS}/linhas`;

  private endLinhaUnica: string = `${Configurations.MDR_API_ADDRESS}/linha/`;

  private endLinhaCompleta: string = `${Configurations.MDR_API_ADDRESS}/linhaCompleta/`;

  constructor(private http: HttpClient) { }

  procurarLinhas(pagina: number, tamanho: number, sort?: string, nome?: string, isDesc?: boolean): Observable<DetalhesPesquisaDTO<LinhaDTO>> {
    const aux: string = this.escolherSort(sort);
    const nomeAux: string = nome? nome : '';
    const order: string = isDesc? 'desc' : 'asc';
    return this.http.get<DetalhesPesquisaDTO<LinhaDTO>>(this.enderecoHttp, { 
        observe: 'body',
        responseType: 'json',
        params: {
          page: '' + pagina,
          size: '' + tamanho,
          sort: aux,
          nome: nomeAux,
          order: order
        }
      });
  }

  procurarPercursosLinha(codigo: string){
    return this.http.get<DetalhesPesquisaDTO<PercursoDTO[]>>(this.endLinhaUnica + codigo, { 
      observe: 'body',
      responseType: 'json'
    });
  }

  procurarLinha(codigo: string): Observable<Linha> {
    return new Observable<Linha>(observer => {
        this.http.get<LinhaCompletaDTO>(this.endLinhaCompleta + codigo, {
          observe: 'body',
          responseType: 'json'
        }).subscribe((results: LinhaCompletaDTO) => {
            const linha = new Linha();
            linha.codigo = results.codigo;
            linha.nome = results.nome;
            linha.percursosIda = results.percursosIda;
            linha.percursosVolta = results.percursosVolta;
            linha.percursosReforco = results.percursosReforco;
            linha.percursosVazios = results.percursosVazios;
            linha.tiposTripulantes = results.tiposTripulantes;
            linha.tiposViaturas = results.tiposViaturas;
            linha.restricaoTiposViaturas = results.restricaoTiposViaturas;
            linha.restricaoTiposTripulantes = results.restricaoTiposTripulantes;
            observer.next(linha);
        }, error => observer.error(error));
    });
  }

  enviarLinha(pedido: PedidoCriarLinhaDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }

  private escolherSort(sort?: string): string {
    if(sort != null && sort != ''){
      switch (sort){
        case 'DATA_DE_ADICAO':
          return 'date';
        case 'CODIGO':
          return 'codigo';
        case 'DESCRICAO':
          return 'nome';
      }
    }
    return '';
  }
}