
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/rede/pedidos/DetalhesOperacaoDTO';
import { ImportationDetailsDTO } from 'src/app/models/viagem/importation/ImportationDetailsDTO';
import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AcessoImportacaoService {

  private enderecoHttp: string = `${Configurations.MDR_API_ADDRESS}/importacao`;

  private mdvImportationAddress: string = `${Configurations.MDV_API_ADDRESS}/importation`;

  constructor(private http: HttpClient) { }

  enviarFicheiro(ficheiro: File): Observable<DetalhesOperacaoDTO[]> {
    const formData: FormData = new FormData();
    formData.append('ficheiro', ficheiro, ficheiro.name);
    return this.http.post<DetalhesOperacaoDTO[]>(this.enderecoHttp, formData, {
      observe: 'body'
    });
  }

  sendTravelFile(file: File): Observable<ImportationDetailsDTO> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<ImportationDetailsDTO>(this.mdvImportationAddress, formData, {
      observe: 'body'
    });
  }
}