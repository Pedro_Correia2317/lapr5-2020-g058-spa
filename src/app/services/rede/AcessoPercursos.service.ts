
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/rede/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarPercursoDTO } from 'src/app/models/rede/percursos/PedidoCriarPercursoDTO';
import { SegmentoRedeDTO } from 'src/app/models/rede/percursos/SegmentoRedeDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { PercursoDTO } from 'src/app/models/rede/percursos/PercursoDTO';
import { Percurso } from 'src/app/models/rede/percursos/Percurso';
import { PercursoCompletoDTO } from 'src/app/models/rede/percursos/PercursoCompletoDTO';
import { SegmentoRede } from 'src/app/models/rede/percursos/SegmentoRede';

@Injectable({
  providedIn: 'root'
})
export class AcessoPercursosService {

  private enderecoHttp: string = `${Configurations.MDR_API_ADDRESS}/percursos`;

  private endPercursoUnico: string = `${Configurations.MDR_API_ADDRESS}/percurso/`;

  private endPercursoCompleto: string = `${Configurations.MDR_API_ADDRESS}/percursoCompleto/`;

  constructor(private http: HttpClient) { }

  procurarPercursos(pagina: number, tamanho: number, sort?: string, desc?: string, isDesc?: boolean): Observable<DetalhesPesquisaDTO<PercursoDTO>> {
    const aux: string = this.escolherSort(sort);
    const descAux: string = desc? desc : '';
    const order: string = isDesc? 'desc' : 'asc';
    return this.http.get<DetalhesPesquisaDTO<PercursoDTO>>(this.enderecoHttp, {
      observe: 'body',
      responseType: 'json',
      params: {
        page: '' + pagina,
        size: '' + tamanho,
        sort: aux,
        descricao: descAux,
        order: order
      }
    });
  }

  procurarSegmentosPercurso(codigo: string): Observable<DetalhesPesquisaDTO<SegmentoRedeDTO>>{
    return this.http.get<DetalhesPesquisaDTO<SegmentoRedeDTO>>(this.endPercursoUnico + codigo, {
      observe: 'body',
      responseType: 'json'
    });
  }

  enviarPercurso(pedido: PedidoCriarPercursoDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }
  
  procurarPercurso(codigo: string): Observable<Percurso> {
    return new Observable<Percurso>(observer => {
        this.http.get<PercursoCompletoDTO>(this.endPercursoCompleto + codigo, {
          observe: 'body',
          responseType: 'json'
        }).subscribe((results: PercursoCompletoDTO) => {
            const perc = new Percurso();
            perc.codigo = results.codigo;
            perc.descricao = results.descricao;
            perc.distanciaTotal = results.distanciaTotal;
            perc.duracaoTotal = results.duracaoTotal;
            perc.segmentos = [];
            results.segmentos.forEach((segDTO) => {
              const seg = new SegmentoRede();
              seg.abreviaturaNoInicio = segDTO.abreviaturaNoInicio;
              seg.abreviaturaNoFim = segDTO.abreviaturaNoFim;
              seg.distancia = segDTO.distancia;
              seg.duracao = segDTO.duracao;
              seg.sequencia = segDTO.sequencia;
              perc.segmentos.push(seg);
            });
            observer.next(perc);
        }, error => observer.error(error));
    });
  }
  
  private escolherSort(sort?: string): string {
    if(sort != null && sort != ''){
      switch (sort){
        case 'DATA_DE_ADICAO':
          return 'date';
        case 'CODIGO':
          return 'codigo';
        case 'DESCRICAO':
          return 'descricao';
        case 'DISTANCIA_TOTAL':            
          return 'distanciaTotal';
        case 'DURACAO_TOTAL':
          return 'duracaoTotal';
      }
    }
    return '';
  }
}