
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateTripRequestDTO } from 'src/app/models/viagem/trip/CreateTripRequestDTO';
import { Trip } from 'src/app/models/viagem/trip/Trip';
import { TripDTO } from 'src/app/models/viagem/trip/TripDTO';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';

import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AccessTripsService {

  private TripsAddress: string = `${Configurations.MDV_API_ADDRESS}/trips`;

  constructor(private http: HttpClient) { }

  searchTrips(page: number, size: number, sort?: string, isDescending?: boolean, filter?: string, onlyUnbindeds?: boolean): Observable<QueryResultsDTO<Trip>> {
    const aux: string = this.chooseSort(sort);
    const order: string = isDescending? 'desc' : 'asc';
    const boolAux = onlyUnbindeds? 'true' : 'false';
    const auxFilter: string = this.chooseFilter(filter);
    return new Observable<QueryResultsDTO<Trip>>(observer => {
        this.http.get<QueryResultsDTO<TripDTO>>(this.TripsAddress, {
          observe: 'body',
          responseType: 'json',
          params: {
            page: '' + page,
            size: '' + size,
            sort: aux,
            order: order,
            filter: auxFilter,
            onlyUnbindeds: boolAux
          }
        }).subscribe((results: QueryResultsDTO<TripDTO>) => {
            const query = new QueryResultsDTO<Trip>();
            query.totalNumber = results.totalNumber;
            query.results = [];
            results.results.forEach(dto => query.results.push(this.mapDTOToTrip(dto)));
            observer.next(query);
        }, error => observer.error(error));
    });
  }

  sendTrip(pedido: CreateTripRequestDTO): Observable<Trip> {
      return new Observable<Trip>(observer => {
          this.http.post<TripDTO>(this.TripsAddress, pedido, {
              observe: 'body',
              responseType: 'json'
          }).subscribe((dto: TripDTO) => observer.next(this.mapDTOToTrip(dto)), error => observer.error(error));
      });
  }

  private mapDTOToTrip(dto: TripDTO): Trip {
    const trip = new Trip();
    trip.code = dto.tripCode;
    trip.line = dto.lineCode;
    trip.path = dto.pathCode;
    trip.startHour = dto.starHour;
    return trip;
  }
    
  private chooseSort(sort?: string): string {
      if(sort != null && sort != ''){
        switch (sort){
          case 'INSERTION_DATE':
            return 'date';
          case 'HOUR':
            return 'hour';
          case 'LINE':            
            return 'line';
          case 'PATH':
            return 'path';
        }
      }
      return '';
  }
    
  private chooseFilter(filter?: string): string {
      if(filter != null && filter != '' && filter != 'NONE'){
        const hours = filter.split(" -> ");
        const hour1 = this.findHour(hours[0]);
        const hour2 = this.findHour(hours[1]);
        return hour1 + '_' + hour2;
      }
      return 'none';
  }

  private findHour(text: string): string {
      const data = text.split("h");
      const hour = data[0];
      const minutes = (data[1].split("m"))[0];
      return hour + ':' + minutes;
  }
}