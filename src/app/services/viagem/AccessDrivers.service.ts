import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Configurations } from "src/app/config/Configurations";
import { CreateDriverRequestDTO } from "src/app/models/viagem/driver/CreateDriverRequestDTO";
import { Driver } from "src/app/models/viagem/driver/Driver";
import { DriverDTO } from "src/app/models/viagem/driver/DriverDTO";
import { QueryResultsDTO } from "src/app/models/viagem/utils/QueryResultsDTO";

@Injectable({
    providedIn: 'root'
  })
  export class AccessDriversService {
  
    private driversAddress: string = `${Configurations.MDV_API_ADDRESS}/drivers`;
  
    constructor(private http: HttpClient) { }
  
    sendVehicle(pedido: CreateDriverRequestDTO): Observable<Driver> {
        return new Observable<Driver>(observer => {
            this.http.post<DriverDTO>(this.driversAddress, pedido, {
                observe: 'body',
                responseType: 'json'
            }).subscribe((dto: DriverDTO) => {
                observer.next(this.mapDTOToDriver(dto));
            }, error => observer.error(error));
        });
    }

    searchDrivers(page: number, size: number, sort?: string, search?: string, isDescending?: boolean) {
        const aux: string = this.chooseSort(sort);
        const order: string = isDescending? 'desc' : 'asc';
        let params: {[param: string]: string | string[];} = {
          page: '' + page,
          size: '' + size,
          sort: aux,
          order: order
        };
        if(search){
          params.search = search;
        }
        return new Observable<QueryResultsDTO<Driver>>(observer => {
            this.http.get<QueryResultsDTO<DriverDTO>>(this.driversAddress, {
              observe: 'body',
              responseType: 'json',
              params: params
            }).subscribe((results: QueryResultsDTO<DriverDTO>) => {
                const query = new QueryResultsDTO<Driver>();
                query.totalNumber = results.totalNumber;
                query.results = [];
                results.results.forEach(dto => query.results.push(this.mapDTOToDriver(dto)));
                observer.next(query);
            }, error => observer.error(error));
        });
    }

    private mapDTOToDriver(dto: DriverDTO): Driver {
        const driver = new Driver();
        driver.mecanographicNumber = dto.mecNumber;
        driver.name = dto.driverName;
        driver.birthDate = dto.birthDate;
        driver.ccNumber = dto.cCNumb;
        driver.nif = dto.nIF;
        driver.initialContractDate = dto.initDate;
        driver.endContractDate = dto.endDate;
        driver.driverTypeCodes = dto.driverTypeCodes? dto.driverTypeCodes : [];
        return driver;
    }
    
    private chooseSort(sort?: string): string {
        if(sort != null && sort != ''){
          switch (sort){
            case 'INSERTION_DATE':
              return 'date';
            case 'NUMBER':
              return 'number';
            case 'NAME':            
              return 'name';
            case 'BIRTH_DATE':
              return 'birth_date';
            case 'START_CONTRACT_DATE':
              return 'start_contract_date';
            case 'END_CONTRACT_DATE':
              return 'end_contract_date';
          }
        }
        return '';
    }
  }