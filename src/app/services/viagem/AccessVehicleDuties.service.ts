
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { CreateVehicleDutyRequestDTO } from 'src/app/models/viagem/vehicleDuty/CreateVehicleDutyRequestDTO';
import { VehicleDuty, VehicleDutyTrip } from 'src/app/models/viagem/vehicleDuty/VehicleDuty';
import { VehicleDutyDTO } from 'src/app/models/viagem/vehicleDuty/VehicleDutyDTO';
import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AccessVehicleDutiesService {

  private TripsAddress: string = `${Configurations.MDV_API_ADDRESS}/vehicleDuties`;

  constructor(private http: HttpClient) { }

  searchVehicleDuties(page: number, size: number, sort?: string, isDescending?: boolean, onlyUnbindeds?: boolean): Observable<QueryResultsDTO<VehicleDuty>> {
    const aux: string = this.chooseSort(sort);
    const order: string = isDescending? 'desc' : 'asc';
    const boolAux = onlyUnbindeds? 'true' : 'false';
    return new Observable<QueryResultsDTO<VehicleDuty>>(observer => {
        this.http.get<QueryResultsDTO<VehicleDutyDTO>>(this.TripsAddress, {
          observe: 'body',
          responseType: 'json',
          params: {
            page: '' + page,
            size: '' + size,
            sort: aux,
            order: order,
            onlyUnbindeds: boolAux
          }
        }).subscribe((results: QueryResultsDTO<VehicleDutyDTO>) => {
            const query = new QueryResultsDTO<VehicleDuty>();
            query.totalNumber = results.totalNumber;
            query.results = [];
            results.results.forEach(dto => query.results.push(this.mapDTOToVehicleDuty(dto)));
            observer.next(query);
        }, error => observer.error(error));
    });
  }

  sendVehicleDuty(pedido: CreateVehicleDutyRequestDTO): Observable<VehicleDuty> {
      return new Observable<VehicleDuty>(observer => {
          this.http.post<VehicleDutyDTO>(this.TripsAddress, pedido, {
              observe: 'body',
              responseType: 'json'
          }).subscribe((dto: VehicleDutyDTO) => observer.next(this.mapDTOToVehicleDuty(dto)), error => observer.error(error));
      });
  }

  private mapDTOToVehicleDuty(dto: VehicleDutyDTO): VehicleDuty {
    const vehicleDuty = new VehicleDuty();
    vehicleDuty.code = dto.code;
    vehicleDuty.listTrips = [];
    vehicleDuty.startTime = new Date(dto.trips[0].startTimeSeconds * 1000).toISOString().substr(11, 8);
    vehicleDuty.endTime = new Date(dto.trips[dto.trips.length-1].endTimeSeconds * 1000).toISOString().substr(11, 8);
    dto.trips.forEach(tripDTO => {
      const vdt: VehicleDutyTrip = new VehicleDutyTrip();
      vdt.tripCode = tripDTO.tripCode;
      vehicleDuty.listTrips.push(vdt);
    });
    return vehicleDuty;
  }

  private chooseSort(sort?: string): string {
    if(sort != null && sort != ''){
      switch (sort){
        case 'INSERTION_DATE':
          return 'date';
        case 'CODE':
          return 'code';
        case 'START_TIME':            
          return 'start_time';
        case 'END_TIME':
          return 'end_time';
        case 'NUMBER_OF_TRIPS':
          return 'number_trips';
      }
    }
    return '';
  }
}