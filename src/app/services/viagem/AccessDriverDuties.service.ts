
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateDriverDutyRequestDTO } from 'src/app/models/viagem/driverDuty/CreateDriverDutyRequestDTO';
import { DriverDuty } from 'src/app/models/viagem/driverDuty/DriverDuty';
import { DriverDutyDTO } from 'src/app/models/viagem/driverDuty/DriverDutyDTO';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AccessDriverDutiesService {

  private TripsAddress: string = `${Configurations.MDV_API_ADDRESS}/driverDuties`;

  constructor(private http: HttpClient) { }

  searchDriverDuties(page: number, size: number, sort?: string): Observable<QueryResultsDTO<DriverDuty>> {
    const aux: string = sort? sort : '';
    return new Observable<QueryResultsDTO<DriverDuty>>(observer => {
        this.http.get<QueryResultsDTO<DriverDutyDTO>>(this.TripsAddress, {
          observe: 'body',
          responseType: 'json',
          params: {
            page: '' + page,
            size: '' + size,
            sort: aux
          }
        }).subscribe((results: QueryResultsDTO<DriverDutyDTO>) => {
            const query = new QueryResultsDTO<DriverDuty>();
            query.totalNumber = results.totalNumber;
            query.results = [];
            results.results.forEach(dto => query.results.push(this.mapDTOToDriverDuty(dto)));
            observer.next(query);
        }, error => observer.error(error));
    });
  }

  sendDriverDuty(pedido: CreateDriverDutyRequestDTO): Observable<DriverDuty> {
      return new Observable<DriverDuty>(observer => {
          this.http.post<DriverDutyDTO>(this.TripsAddress, pedido, {
              observe: 'body',
              responseType: 'json'
          }).subscribe((dto: DriverDutyDTO) => observer.next(this.mapDTOToDriverDuty(dto)), error => observer.error(error));
      });
  }

  private mapDTOToDriverDuty(dto: DriverDutyDTO): DriverDuty {
    const driverDuty = new DriverDuty();
    driverDuty.code = dto.code;
    driverDuty.workBlockCodes = dto.workBlocks;
    return driverDuty;
  }
}