
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Configurations } from "src/app/config/Configurations";
import { QueryResultsDTO } from "src/app/models/viagem/utils/QueryResultsDTO";
import { CreateWorkBlockRequestDTO } from "src/app/models/viagem/workblocks/CreateWorkBlockRequestDTO";
import { WorkBlock, WorkBlockTrip } from "src/app/models/viagem/workblocks/WorkBlock";
import { WorkBlockDTO } from "src/app/models/viagem/workblocks/WorkBlockDTO";

@Injectable({
    providedIn: 'root'
  })
  export class AccessWorkBlocksService {
  
    private workBlocksAddress: string = `${Configurations.MDV_API_ADDRESS}/workBlocks`;
  
    constructor(private http: HttpClient) { }
  
    searchWorkBlocks(page: number, size: number, sort?: string, onlyUnbindeds?: boolean): Observable<QueryResultsDTO<WorkBlock>> {
      const aux: string = sort? sort : '';
      const boolAux = onlyUnbindeds? 'true' : 'false';
      return new Observable<QueryResultsDTO<WorkBlock>>(observer => {
          this.http.get<QueryResultsDTO<WorkBlockDTO>>(this.workBlocksAddress, {
            observe: 'body',
            responseType: 'json',
            params: {
              page: '' + page,
              size: '' + size,
              sort: aux,
              onlyUnbindeds: boolAux
            }
          }).subscribe((results: QueryResultsDTO<WorkBlockDTO>) => {
              const query = new QueryResultsDTO<WorkBlock>();
              query.totalNumber = results.totalNumber;
              query.results = [];
              results.results.forEach(dto => query.results.push(this.mapDTOToWorkBlock(dto)));
              observer.next(query);
          }, error => observer.error(error));
      });
    }
  
    sendWorkBlock(pedido: CreateWorkBlockRequestDTO): Observable<WorkBlock[]> {
        return new Observable<WorkBlock[]>(observer => {
            this.http.post<WorkBlockDTO[]>(this.workBlocksAddress, pedido, {
                observe: 'body',
                responseType: 'json'
            }).subscribe((dto: WorkBlockDTO[]) => {
              const list: WorkBlock[] = [];
              dto.forEach((wb) => list.push(this.mapDTOToWorkBlock(wb)));
              observer.next(list);
            }, error => observer.error(error));
        });
    }
  
    private mapDTOToWorkBlock(dto: WorkBlockDTO): WorkBlock {
      const workBlock = new WorkBlock();
      workBlock.code = dto.code;
      workBlock.listTrips = [];
      if(dto.trips){
        dto.trips.forEach(vdDTO => {
          const wbt: WorkBlockTrip = new WorkBlockTrip();
          wbt.dutyCode = vdDTO.dutyCode;
          wbt.durationSeconds = vdDTO.durationSeconds;
          workBlock.listTrips.push(wbt);
        });
      }
      return workBlock;
    }
  }