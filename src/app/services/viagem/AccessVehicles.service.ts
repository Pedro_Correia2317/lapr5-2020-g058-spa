
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Configurations } from '../../config/Configurations';
import { CreateVehicleRequestDTO } from 'src/app/models/viagem/vehicle/CreateVehicleRequestDTO';
import { VehicleDTO } from 'src/app/models/viagem/vehicle/VehicleDTO';
import { Vehicle } from 'src/app/models/viagem/vehicle/Vehicle';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';

@Injectable({
  providedIn: 'root'
})
export class AccessVehiclesService {

  private vehiclesAddress: string = `${Configurations.MDV_API_ADDRESS}/vehicles`;

  constructor(private http: HttpClient) { }

  sendVehicle(pedido: CreateVehicleRequestDTO): Observable<Vehicle> {
      return new Observable<Vehicle>(observer => {
          this.http.post<VehicleDTO>(this.vehiclesAddress, pedido, {
              observe: 'body',
              responseType: 'json'
          }).subscribe((dto: VehicleDTO) => {
              observer.next(this.mapDTOToVehicle(dto));
          }, error => observer.error(error));
      });
  }

  searchVehicles(page: number, size: number, sort?: string, isDescending?: boolean) {
      const aux: string = this.chooseSort(sort);
      const order: string = isDescending? 'desc' : 'asc';
      return new Observable<QueryResultsDTO<Vehicle>>(observer => {
          this.http.get<QueryResultsDTO<VehicleDTO>>(this.vehiclesAddress, {
            observe: 'body',
            responseType: 'json',
            params: {
              page: '' + page,
              size: '' + size,
              sort: aux,
              order: order
            }
          }).subscribe((results: QueryResultsDTO<VehicleDTO>) => {
              const query = new QueryResultsDTO<Vehicle>();
              query.totalNumber = results.totalNumber;
              query.results = [];
              results.results.forEach(dto => query.results.push(this.mapDTOToVehicle(dto)));
              observer.next(query);
          }, error => observer.error(error));
      });
  }

  private mapDTOToVehicle(dto: VehicleDTO): Vehicle {
    const vehicle = new Vehicle();
    vehicle.licensePlate = dto.licensePlate;
    vehicle.vin = dto.vin;
    vehicle.entryDate = dto.entryDate;
    vehicle.vehicleType = dto.typeCode;
    return vehicle;
  }
  
  private chooseSort(sort?: string): string {
      if(sort != null && sort != ''){
        switch (sort){
          case 'INSERTION_DATE':
            return 'date';
          case 'LICENSE_PLATE':
            return 'license_plate';
          case 'ENTRY_DATE':            
            return 'entry_date';
          case 'VIN':
            return 'vin';
        }
      }
      return '';
  }
}