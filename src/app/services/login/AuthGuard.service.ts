import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './Auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  private role: string;

  constructor(public authService: AuthService, public router: Router)
  {
    this.authService.role.subscribe(
      (role: string)  => this.role = role
    );
  }

  async canActivate(route: ActivatedRouteSnapshot) {
    let roles = route.data.roles as Array<string>;
    if (!this.authService.isUserAuthenticated() || !roles.includes(this.role)) {
      await this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}