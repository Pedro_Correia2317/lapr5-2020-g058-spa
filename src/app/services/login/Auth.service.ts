import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/login/User';
import { LoginRequestDTO } from 'src/app/models/login/LoginRequestDTO';
import { LoginDTO } from 'src/app/models/login/LoginDTO';
import { Configurations } from 'src/app/config/Configurations';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginAddress: string = `${Configurations.MDV_API_ADDRESS}/login`;

  private loginCookieName: string = 'id_token';

  private roleCookieName: string = 'id_role';

  private usernameCookieName: string = 'id_username';

  public isAuthenticated = new BehaviorSubject<boolean>(false);

  public role = new BehaviorSubject<string>("");

  private username: string;

  private jwtToken: string;

  constructor(private cookies: CookieService, private http: HttpClient, private router: Router) {
    this.jwtToken = "";
    this.username = "";
  }

  checkAuthentication(): void {
    const jwtToken: string = this.cookies.get(this.loginCookieName);
    const roleToken: string = this.cookies.get(this.roleCookieName);
    const usernameToken: string = this.cookies.get(this.usernameCookieName);
    if(jwtToken != null && jwtToken != "" && roleToken != null && roleToken != "" && usernameToken != null && usernameToken != ""){
      this.isAuthenticated.next(true);
      this.role.next(roleToken);
      this.jwtToken = jwtToken;
      this.username = usernameToken;
    }
  }

  isUserAuthenticated(): boolean {
    return this.jwtToken != null && this.jwtToken != "";
  }

  login(request: LoginRequestDTO): Observable<User> {
    return new Observable<User>(observer => {
        this.http.post<LoginDTO>(this.loginAddress, request, {
            observe: 'body',
            responseType: 'json'
        }).subscribe((dto: LoginDTO) => {
            const user = new User();
            user.username = dto.username;
            user.role = dto.role;
            this.jwtToken = dto.token;
            this.cookies.set(this.loginCookieName, dto.token, 0.08333);
            this.cookies.set(this.roleCookieName, user.role, 0.08333);
            this.cookies.set(this.usernameCookieName, user.username, 0.08333);
            observer.next(user);
            this.isAuthenticated.next(true);
            this.username = user.username;
            this.role.next(user.role);
            this.router.navigate(["/"]);
        }, error => observer.error(error));
    });
  }

  logout(redirect: string) {
    this.jwtToken = "";
    this.isAuthenticated.next(false);
    this.role.next("");
    this.cookies.delete(this.loginCookieName);
    this.cookies.delete(this.roleCookieName);
    this.cookies.delete(this.usernameCookieName);
    this.router.navigate([redirect]);
  }

  public getJWTToken(){
    return this.jwtToken;
  }

  public getUser(){
    return this.username;
  }

}