
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Configurations } from '../../config/Configurations';
import { User } from 'src/app/models/login/User';
import { CreateUserRequestDTO } from 'src/app/models/login/CreateUserRequestDTO';
import { UserDTO } from 'src/app/models/login/UserDTO';
import { UpdateUserRequestDTO } from 'src/app/models/login/UpdateUserRequestDTO';

@Injectable({
  providedIn: 'root'
})
export class AcessUsersService {

  private usersAddress: string = `${Configurations.MDV_API_ADDRESS}/users/`;

  constructor(private http: HttpClient) { }

  sendUser(pedido: CreateUserRequestDTO): Observable<User> {
      return new Observable<User>(observer => {
          this.http.post<UserDTO>(this.usersAddress, pedido, {
              observe: 'body',
              responseType: 'json'
          }).subscribe((dto: UserDTO) => {
              const user = new User();
              user.username = dto.username;
              user.email = dto.email;
              user.dateOfBirth = dto.birthDate;
              user.role = dto.role;
              observer.next(user);
          }, error => observer.error(error));
      });
  }

  updateUser(dto: UpdateUserRequestDTO, username: string): Observable<string> {
      return this.http.put<string>(this.usersAddress + username, dto, 
        {
          observe: 'body',
          responseType: 'json'
        }
      );
  }

  deleteUser(username: string): Observable<string> {
    return this.http.delete<string>(this.usersAddress + username, 
      {
        observe: 'body',
        responseType: 'json'
      }
    );
  }
}