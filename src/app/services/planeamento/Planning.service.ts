
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NodeInRoute } from 'src/app/models/planeamento/NodeInRoute';
import { NodeInRouteDTO } from 'src/app/models/planeamento/NodeInRouteDTO';
import { DriverDuty } from 'src/app/models/viagem/driverDuty/DriverDuty';
import { DriverDutyDTO } from 'src/app/models/viagem/driverDuty/DriverDutyDTO';
import { PlanningDriverDutyDTO } from 'src/app/models/viagem/driverDuty/PlanningDriverDutyDTO';

import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {

  private shortestRouteAddress: string = `${Configurations.PLAN_API_ADDRESS}/shortest_path`;

  private schedulingAddress: string = `${Configurations.PLAN_API_ADDRESS}/schedule_driver_duties`;

  constructor(private http: HttpClient) { }

  findShortestRoute(orig_node: string, dest_node: string, current_time: number): Observable<NodeInRoute[]> {
    const aux: string = orig_node? orig_node : '';
    const aux2: string = dest_node? dest_node : '';
    return new Observable<NodeInRoute[]>(observer => {
        this.http.get<NodeInRouteDTO[]>(this.shortestRouteAddress, {
          observe: 'body',
          responseType: 'json',
          params: {
            orig_node: '' + aux,
            end_node: '' + aux2,
            current_time: '' + current_time
          }
        }).subscribe((results: NodeInRouteDTO[]) => {
            const aux: NodeInRoute[] = [];
            results.forEach(nodeDTO => {
                const node = new NodeInRoute();
                node.node = nodeDTO.node;
                node.time = new Date(nodeDTO.time * 1000).toISOString().substr(11, 8);
                aux.push(node);
            });
            observer.next(aux);
        }, error => observer.error(error));
    });
  }

  scheduleDriverDuties(numDrivers: number, chanceCross: number, chanceMut: number, pop: number, nGen: number): Observable<DriverDuty[]> {
    return new Observable<DriverDuty[]>(observer => {
        this.http.get<PlanningDriverDutyDTO[]>(this.schedulingAddress, {
          observe: 'body',
          responseType: 'json',
          params: {
            number_drivers: '' + numDrivers,
            chance_crossing: '' + chanceCross,
            chance_mutation: '' + chanceMut,
            population: '' + pop,
            n_generations: '' + nGen
          }
        }).subscribe((results: PlanningDriverDutyDTO[]) => {
            const aux: DriverDuty[] = [];
            results.forEach(ddDto => {
                const driverDuty = new DriverDuty();
                driverDuty.code = '' + ddDto.number;
                driverDuty.workBlockCodes = [];
                ddDto.list_work_blocks.forEach(wb => driverDuty.workBlockCodes.push(wb.code))
                aux.push(driverDuty);
            });
            observer.next(aux);
        }, error => observer.error(error));
    });
  }
}