
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { VisualizacaoRedeComponent } from './visualizacao-rede.component';
import { Visualization2dComponent } from './visualization2d/visualization2d.component';
import { Visualization3dComponent } from './visualization3d/visualization3d.component';

const routes: Routes = [
  {path: '', redirectTo: '2d'},
  {path: '', component: VisualizacaoRedeComponent, 
    children: [
      { path: '2d', component: Visualization2dComponent },
      { path: '3d', component: Visualization3dComponent }
    ]
  }
];

@NgModule({
  declarations: [VisualizacaoRedeComponent, Visualization2dComponent, Visualization3dComponent],
  imports: [CommonModule, TranslateModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoRedeModule { }