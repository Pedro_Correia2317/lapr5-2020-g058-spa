
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NoDTO } from 'src/app/models/rede/no/NoDTO';
import { GUI } from "dat.gui";
import { AcessoNosService } from 'src/app/services/rede/AcessoNos.service';
import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { Configurations } from 'src/app/config/Configurations';
import * as THREE from 'three';

declare var harp: any;

@Component({
  selector: 'app-visualization3d',
  templateUrl: './visualization3d.component.html',
  styleUrls: ['./visualization3d.component.css']
})
export class Visualization3dComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild("canvas", { static: false })
  private mapElement: ElementRef;

  private map: any;

  private light: THREE.DirectionalLight;

  private car: FirstPersonModel;

  private nodes: THREE.Group[] = [];

  private mouse: THREE.Vector2;

  private raycaster: THREE.Raycaster = new THREE.Raycaster();

  tooltipPosition: {x: string, y: string};

  tooltipData: NoDTO | undefined;

  constructor(private todosNos: AcessoNosService) {
    this.mouse = new THREE.Vector2();
  }

    public ngOnDestroy(): void {
        this.map.dispose();
    }

  public ngOnInit(): void {
  }
  
  public ngAfterViewInit(): void {
    this.initializeMap();
    this.todosNos.procurarNos(1, Number.MAX_SAFE_INTEGER).subscribe((detalhes) => {
        this.loadNodesModels(detalhes.lista);
    });
    this.initializeFirstPersonView();
  }

  private initializeMap(): void {
      const canvas = this.mapElement.nativeElement;
      const theme = THEME;
      const map = new harp.MapView({
          canvas,
          theme,
          enableShadows: true
        });
      map.renderLabels = false;
      map.fog.enabled = false;
      const mapControls = new harp.MapControls(map);
      mapControls.maxTiltAngle = 90;
      map.lookAt({ target: HERE, zoomLevel: 19 });
      map.resize(window.innerWidth, window.innerHeight);
      window.addEventListener("resize", () => {
          map.resize(window.innerWidth, window.innerHeight);
      });
      this.map = map;
      this.setupTilesAndIlumination();
  }

  private setupTilesAndIlumination(){
    const omvDataSource = new harp.OmvDataSource({
        //baseUrl: "https://vector.hereapi.com/v2/vectortiles/base/mc",
        //authenticationCode: "iDIAD7Oz93SEDyNqzr-SWDAQ4ovlrnxy3cLpYLPGTds",
        baseUrl: "https://xyz.api.here.com/tiles/herebase.02",
        apiFormat: harp.APIFormat.XYZOMV,
        styleSetName: "tilezen",
        authenticationCode: "APK3tlhkRISfzJXergBpkwA"
    });
    this.map.addDataSource(omvDataSource).then(() => {
        const light = this.map.lights.find((item: THREE.Light) => item.type === 'DirectionalLight') as | THREE.DirectionalLight | undefined;
        if (light === undefined) {
            throw new Error("Light for a sun was not found.");
        }
        this.light = light;
        this.map.addEventListener(harp.MapViewEventNames.MovementFinished, this.update);
        this.addGuiElements();
        this.update();
    });

  }

  private addGuiElements(): void {
    const gui = new GUI({ width: 300, autoPlace: false });
    gui.add(this.guiOptions, "lightX", -3000, 3000, 100);
    gui.add(this.guiOptions, "lightY", -3000, 3000, 100);
    gui.add(this.guiOptions, "lightZ", -3000, 3000, 100);
    gui.add(this.guiOptions, "camera");
    const guiContainer = document.getElementById("gui-container");
    if(guiContainer != null){
        guiContainer.appendChild(gui.domElement);
    }
  }

  private loadNodesModels(listNodes: NoDTO[]): void {
    const loader = new GLTFLoader();
    listNodes.forEach((node) => {
        loader.load(Configurations.ASSETS_LOCATION + node.abreviatura + '.glb', 
            gltf => {
                this.placeExternalModelOnMap(gltf, node);
            }, 
            undefined, 
            error => loader.load(Configurations.DEFAULT_MODEL, gltf => this.placeExternalModelOnMap(gltf, node))
        );
    });
  }

  private placeExternalModelOnMap(gltf: any, node: NoDTO): void {
      const model = gltf.scene;
      gltf.scene.traverse((child: any) => child.castShadow = true);
      model.renderOrder = 100;
      model.anchor = new harp.GeoCoordinates(node.latitude, node.longitude, 50);
      model.scale.multiplyScalar(300);
      model.rotateX(Math.PI/2);
      model.children[0].userData = [node];
      this.nodes.push(model.children[0]);
      this.map.mapAnchors.add(model);
  }

  private placeCarOnMap(gltf: any, lat:number, lon: number): void {
      const model = gltf.scene;
      gltf.scene.traverse((child: any) => child.castShadow = true);
      model.renderOrder = 100;
      model.anchor = new harp.GeoCoordinates(lat, lon, 50);
      model.scale.multiplyScalar(300);
      model.rotateX(Math.PI/2);
      this.map.mapAnchors.add(model);
  }
  
  update = (): void => {
    requestAnimationFrame(this.update);
    const lightPos = this.light.position;

    const r = this.map.targetDistance;
    lightPos.setX(this.guiOptions.lightX);
    lightPos.setY(this.guiOptions.lightY);
    lightPos.setZ(this.guiOptions.lightZ);
    // Resetting the target is important, because this is overriden in the MapView.
    // This is an ugly hack and HARP-10353 should improve this.
    this.light.target.position.set(0, 0, -r);

    this.checkKeyPresses();
    this.map.update();
  }

  private initializeFirstPersonView(){
      this.loadCar();
      document.addEventListener("keydown", this.onDocumentKeyDown, false);
      document.addEventListener("keyup", this.onDocumentKeyUp, false);
      this.mapElement.nativeElement.addEventListener('click', this.onMouseClick, false);
  }

  private loadCar(){
    const loader = new GLTFLoader();
    loader.load(Configurations.CAR_MODEL, 
        gltf => {
            this.car = new FirstPersonModel(gltf.scene, this.map);
            gltf.scene.rotateZ(Math.PI);
            this.placeCarOnMap(gltf, FirstPersonModel.CAR_LATITUDE, FirstPersonModel.CAR_LONGITUDE);
        }, 
        undefined, 
        error => console.log(error)
    );
  }
  
  onMouseClick = (event: any) => {
    this.tooltipData = undefined;
    const {top, left, width, height} = this.map.renderer.domElement.getBoundingClientRect();
    this.mouse.x = (( event.clientX - left)  / width  ) * 2 - 1;
    this.mouse.y = - (( event.clientY - top) / height  ) * 2 + 1;
    this.tooltipPosition = {
      x: event.clientX + 'px',
      y: event.clientY + 'px'
    }
    let usableIntersections = this.map.intersectMapObjects(event.clientX, event.clientY);
    /*
    this.raycaster.setFromCamera(this.mouse, this.map.camera);
    const intersects = this.raycaster.intersectObjects(this.nodes);
    intersects.forEach((intersect) => {
      this.tooltipData = intersect.object.userData[0];
    });
    */
  }

  onDocumentKeyDown = (event: any) => {
      switch(event.key){
          case 'ArrowUp': 
            pressedKeys.up = true;
            break;
        case 'ArrowRight': 
            pressedKeys.right = true;
            break;
        case 'ArrowDown':
            pressedKeys.down = true;
            break;
        case 'ArrowLeft': 
            pressedKeys.left = true;
            break;
      }
  }

  onDocumentKeyUp = (event: any) => {
    switch(event.key){
        case 'ArrowUp': 
          pressedKeys.up = false;
          break;
        case 'ArrowRight': 
          pressedKeys.right = false;
          break;
        case 'ArrowDown': 
          pressedKeys.down = false;
          break;
        case 'ArrowLeft': 
          pressedKeys.left = false;
          break;
    }
  }

  /*

  releaseUpArrowOperation(){
      console.log("Up has been released");
      pressedKeys.up = false;
  }
  */

  private checkKeyPresses(){
      if(pressedKeys.up){
          this.car.accelerate();
      }
      if(pressedKeys.left){
          this.car.turnLeft();
      }
      if(pressedKeys.right){
          this.car.turnRight();
      }
      if(pressedKeys.down){
          this.car.deaccelarate();
      }
  }


  private guiOptions = {
      lightX: 1,
      lightY: 10,
      lightZ: 20,
      camera: () => {this.car.updateCamera();}
  };

}

const THEME = {
    extends: "https://unpkg.com/@here/harp-map-theme@0.3.3/resources/berlin_tilezen_base.json",
    lights: [
        {
            type: "ambient",
            color: "#ffffff",
            name: "ambientLight",
            intensity: 0.9
        },
        {
            type: "directional",
            color: "#ffffff",
            name: "light1",
            intensity: 1,
            // Will be overriden immediately, see `update`
            direction: {
                x: 0,
                y: 0.01,
                z: -1
            },
            castShadow: true
        }
    ],
    definitions: {
        // Opaque buildings
        defaultBuildingColor: { value: "#000000" }
    }
};

const HERE = new harp.GeoCoordinates(41.1293363229325, -8.4464785432391);

const pressedKeys = {
    up: false,
    down: false,
    left: false,
    right: false,
};

export class FirstPersonModel {

    private carMesh: any;

    private camera: any;

    private rotationRadians: number;

    public static readonly CAR_LATITUDE = 41.1391362229325;

    public static readonly CAR_LONGITUDE = -8.4464785432391;

    public readonly SPEED = 0.0001;

    public readonly TO_DEGREES = 180/Math.PI;

    constructor(carMesh: any, scene: any){
        this.carMesh = carMesh;
        this.camera = scene;
        this.rotationRadians = 0;
    }

    updateCamera = () =>  {
        this.updateFirstPersonView();
    }

    accelerate = () => {
        this.carMesh.anchor.latitude += this.SPEED * Math.cos(this.rotationRadians);
        this.carMesh.anchor.longitude += this.SPEED * Math.sin(this.rotationRadians);
        this.updateFirstPersonView();
    }

    deaccelarate = () => {
        this.carMesh.anchor.latitude -= this.SPEED * Math.cos(this.rotationRadians);
        this.carMesh.anchor.longitude -= this.SPEED * Math.sin(this.rotationRadians);
        this.updateFirstPersonView();
    }

    updateFirstPersonView = () => {
        const cameraCoordinates = {
            latitude: this.carMesh.anchor.latitude,
            longitude: this.carMesh.anchor.longitude,
            altitude: this.carMesh.anchor.altitude + 25
            //latitude: this.carMesh.anchor.latitude + 0.0018,
            //longitude: this.carMesh.anchor.longitude + 0.00019 * Math.sin(this.rotationRadians),
            //altitude: this.carMesh.anchor.altitude + 25,
        };
        this.camera.lookAt({target: cameraCoordinates, zoomLevel: 22, tilt: 80, heading: this.rotationRadians * this.TO_DEGREES});
    }

    turnRight = () => {
        //if(this.rotationRadians < Math.PI/2){
            this.carMesh.rotation.y += 0.01;
            this.rotationRadians += 0.01;
        //}
        this.updateFirstPersonView();
    }

    turnLeft = () => {
        this.carMesh.rotation.y -= 0.01;
        this.rotationRadians -= 0.01;
        //if(this.rotationRadians > -Math.PI/2){
        //}
        this.updateFirstPersonView();
    }

}