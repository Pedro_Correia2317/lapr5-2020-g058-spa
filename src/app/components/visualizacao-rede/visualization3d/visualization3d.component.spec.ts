import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Visualization3dComponent } from './visualization3d.component';

describe('Visualization3dComponent', () => {
  let component: Visualization3dComponent;
  let fixture: ComponentFixture<Visualization3dComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Visualization3dComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Visualization3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
