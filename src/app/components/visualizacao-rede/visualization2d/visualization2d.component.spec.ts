import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Visualization2dComponent } from './visualization2d.component';

describe('Visualization2dComponent', () => {
  let component: Visualization2dComponent;
  let fixture: ComponentFixture<Visualization2dComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Visualization2dComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Visualization2dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
