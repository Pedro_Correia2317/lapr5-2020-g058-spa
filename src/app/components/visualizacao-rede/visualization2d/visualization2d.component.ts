
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NoDTO } from 'src/app/models/rede/no/NoDTO';
import { CriadorRede, SegmentoAuxiliar } from 'src/app/services/rede/CriadorRede.service';
import * as THREE from 'three';
// @ts-ignore
import * as Mappa from './mappa-mundi'; 

@Component({
  selector: 'app-visualization2d',
  templateUrl: './visualization2d.component.html',
  styleUrls: ['./visualization2d.component.css']
})
export class Visualization2dComponent implements OnInit, AfterViewInit {
  
  private WIDTH = 800;
  private HEIGHT = 500;
  private readonly VIEW_ANGLE = 45;
  private readonly ASPECT = this.WIDTH / this.HEIGHT;
  private readonly NEAR = 0.1;
  private readonly FAR = 10000;
  private readonly WIDTH_QUADRADO = 11;

  private readonly COLORS: number[] = [0x0000ff, 0xfe2e2e, 0x3d8542, 0x004620, 0x64365e, 0x845422, 0x183132, 0xff0000];

  renderer: THREE.WebGLRenderer;

  scene: THREE.Scene;

  camera: THREE.PerspectiveCamera;

  myMap: any;

  dataLoaded: number;

  segmentos: SegmentoAuxiliar[];

  meshes: THREE.Mesh[];

  segments: THREE.Mesh[];

  private mouse = new THREE.Vector2();

  private raycaster: THREE.Raycaster = new THREE.Raycaster();

  message: string;

  tooltipPosition: {x: string, y: string};

  tooltipData: NoDTO | undefined;

  tooltipSegmentData: SegmentoAuxiliar | undefined;

  constructor(
    private criador: CriadorRede,
    private i18n: TranslateService
    ) 
  {
    this.meshes = [];
    this.segments = [];
    this.segmentos = [];
    this.dataLoaded = 0;
  }

  ngAfterViewInit(): void {
    this.iniciarMapa();
    this.criador.procurarTodosNos().subscribe((numNos: NoDTO[]) => {
      const mesh = this.createDefaultMesh();
      for (let index = 0; index < numNos.length; index++) {
        const node = numNos[index];
        node.tipoNo = this.i18n.instant(node.tipoNo);
        mesh.userData = [numNos[index]];
        this.meshes.push(mesh.clone());
      }
      this.dataLoaded += 1;
      this.update();
    });
    this.criador.procurarTodosSegmentos().subscribe((resultados: SegmentoAuxiliar[]) => {
      this.segmentos = resultados;
      this.dataLoaded += 1;
      this.update();
    });
  }

  ngOnInit(): void {
  }

  private createDefaultMesh(): THREE.Mesh {
    const geometry = new THREE.CircleGeometry( this.WIDTH_QUADRADO, 32 );
    const material = new THREE.MeshBasicMaterial( { color: 0xc21b1c } );
    const circle = new THREE.Mesh( geometry, material );
    circle.renderOrder = 100;
    return circle;
  }

  iniciarMapa = () => {
    const canvas = document.getElementById("mapa");
    if(canvas != null) {
      this.WIDTH = canvas.offsetWidth;
      this.HEIGHT = canvas.offsetHeight;
      canvas.addEventListener('click', this.onMouseClick, false);
    }
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(this.VIEW_ANGLE, this.ASPECT, this.NEAR, this.FAR);
    //this.camera = new THREE.PerspectiveCamera();
    this.renderer = new THREE.WebGLRenderer({alpha: true, canvas: canvas as HTMLCanvasElement});
    this.camera.position.z = 300;
    this.scene.add(this.camera);
    this.renderer.setSize(this.WIDTH, this.HEIGHT);
    const light = new THREE.PointLight(0xffffff, 1.2);
    light.position.set(0, 0, 6);
    this.scene.add(light);
    const key = 'pk.eyJ1IjoiMTE4MTIwNCIsImEiOiJja2h0ZjE5bHkwYTBmMnpvMzZnYjdpbWk4In0.pJ_888z0LqKuFx7I_ziu6g';
    const mappa = new Mappa('Mapbox', key);

    var options = {
        lat: 41.2,
        lng: -8.3,
        zoom: 12,
        pitch: 0,
        studio: true,
        style: 'mapbox://styles/mapbox/streets-v11'
    }

    this.myMap = mappa.tileMap(options);
    this.myMap.overlay(canvas);
    this.myMap.onChange(this.update);
    
  }
  
  onMouseClick = (event: any) => {
    this.tooltipData = undefined;
    this.tooltipSegmentData = undefined;
    const {top, left, width, height} = this.renderer.domElement.getBoundingClientRect();
    this.mouse.x = (( event.clientX - left)  / width  ) * 2 - 1;
    this.mouse.y = - (( event.clientY - top) / height  ) * 2 + 1;
    this.tooltipPosition = {
      x: event.clientX + 'px',
      y: event.clientY + 'px'
    }
    let stop = false;
    this.raycaster.setFromCamera(this.mouse, this.camera);
    let intersects = this.raycaster.intersectObjects(this.meshes);
    intersects.forEach((intersect) => {
      this.tooltipData = intersect.object.userData[0];
      stop = true;
    });
    if(stop === false)
    {
      intersects = this.raycaster.intersectObjects(this.segments);
      intersects.forEach((intersect) => {
        this.tooltipSegmentData = intersect.object.userData[0];
      });
    }
  }

  private update = () => {
    if(this.dataLoaded >= 2){
        this.scene.clear();
        this.meshes.forEach((mesh: THREE.Mesh, index: number) => {this.updateMesh(mesh, index)});
        this.segments = [];
        this.segmentos.forEach(segmento => {this.updateTrilho(segmento)});
        this.renderer.render(this.scene, this.camera);
    }
  }

  private updateTrilho(segmento: SegmentoAuxiliar){
    const color = this.COLORS[(segmento.indexlinha%this.COLORS.length)];
    const lineWidth = 0.5;
    const distEntreSegs = 3.5;
    let xa1 = this.meshes[segmento.index1].position.x;
    let xb1 = this.meshes[segmento.index2].position.x;
    let ya1 = this.meshes[segmento.index1].position.y;
    let yb1 = this.meshes[segmento.index2].position.y;
    let hasSwitchedPositions = false;
    if(xb1 < xa1){
      let aux = xa1;
      xa1 = xb1;
      xb1 = aux;
      aux = ya1;
      ya1 = yb1;
      yb1 = aux;
      hasSwitchedPositions = true;
    }
    const distance = Math.sqrt((xb1-xa1)*(xb1-xa1) + (yb1-ya1)*(yb1-ya1));
    const cosBeta = -(yb1 - ya1)/distance;
    const sinBeta = (xb1 - xa1)/distance;
    let d;
    if(segmento.sobreposicao % 2 === 0){
      d = -1 * (segmento.sobreposicao/2);
    } else {
      d = (segmento.sobreposicao+1)/2;
    }
    const xa2 = xa1 + (d * distEntreSegs * cosBeta);
    const ya2 = ya1 + (d * distEntreSegs * sinBeta);
    const xb2 = xb1 + (d * distEntreSegs * cosBeta);
    const yb2 = yb1 + (d * distEntreSegs * sinBeta);
    let points;
    if (ya2 < yb2) {
      points = [new THREE.Vector2(xa2-lineWidth, ya2+lineWidth), new THREE.Vector2(xa2+lineWidth, ya2-lineWidth), new THREE.Vector2(xb2+lineWidth, yb2-lineWidth), new THREE.Vector2(xb2-lineWidth, yb2+lineWidth)];
    } else {
      points = [new THREE.Vector2(xa2-lineWidth, ya2-lineWidth), new THREE.Vector2(xa2+lineWidth, ya2+lineWidth), new THREE.Vector2(xb2+lineWidth, yb2+lineWidth), new THREE.Vector2(xb2-lineWidth, yb2-lineWidth)];
    }
    const geo = new THREE.Shape(points);
    const line = new THREE.Mesh(new THREE.ShapeGeometry(geo), new THREE.LineDashedMaterial({color}));

    const compT = 3;
    const xm = (xa2 + xb2)/2;
    const ym = (ya2 + yb2)/2;
    let alpha;
    if (hasSwitchedPositions) {
      alpha = Math.PI/2 - Math.atan2(ya2 - yb2, xa2 - xb2);
    } else {
      alpha = Math.PI/2 - Math.atan2(yb2 - ya2, xb2 - xa2);
    }
    const A = compT * Math.sin(alpha);
    const B = compT * Math.sin(Math.PI/2 - alpha);
    const pointsTriangulo = [new THREE.Vector2(xm-B, ym+A), new THREE.Vector2(xm+B, ym-A), new THREE.Vector2(xm-A, ym-B)];
    const cabecaLinhaGeo = new THREE.Shape(pointsTriangulo);
    const cabecaLinha = new THREE.Mesh(new THREE.ShapeGeometry(cabecaLinhaGeo), new THREE.LineDashedMaterial({color}));
    cabecaLinha.translateX(d*A);
    cabecaLinha.translateY(d*B);
    line.add(cabecaLinha);
    line.userData = [segmento];
    this.segments.push(line);
    this.scene.add(line);
  }

  private updateMesh(mesh: THREE.Mesh, index: number){
    let pos = this.myMap.latLngToPixel(this.criador.latNo(index) , this.criador.lonNo(index));
    let vector = new THREE.Vector3();
    vector.set((pos.x / this.WIDTH) * 2 - 1, -(pos.y / this.HEIGHT) * 2 + 1, 0.5);
    vector.unproject(this.camera);
    let dir = vector.sub(this.camera.position).normalize();
    let distance = -this.camera.position.z / dir.z;
    let newPos = this.camera.position.clone().add(dir.multiplyScalar(distance));
    mesh.position.set(newPos.x, newPos.y, newPos.z);
    this.scene.add(mesh);
  }
}
