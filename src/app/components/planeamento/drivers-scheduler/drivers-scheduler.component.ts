import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DriverDuty } from 'src/app/models/viagem/driverDuty/DriverDuty';
import { PlanningService } from 'src/app/services/planeamento/Planning.service';

@Component({
  selector: 'app-drivers-scheduler',
  templateUrl: './drivers-scheduler.component.html',
  styleUrls: ['./drivers-scheduler.component.css']
})
export class DriversSchedulerComponent implements OnInit {

  scheduleDriversForm: FormGroup;

  duties: DriverDuty[];

  errorMessages: string[];

  constructor(private translator: TranslateService,
      private builder: FormBuilder,
      private planCom: PlanningService) {
      this.scheduleDriversForm = this.builder.group({
        numberDrivers: 0,
        chanceCrossing: 0,
        chanceMutation: 0,
        population: 0,
        numberGenerations: 0
      });
      this.duties = [];
      this.errorMessages = [];
  }

  ngOnInit(): void {
  }

  onSubmit(formData: any) {
    const nd = formData.numberDrivers;
    const cc = formData.chanceCrossing;
    const cm = formData.chanceMutation;
    const pop = formData.population;
    const ng = formData.numberGenerations;
    this.errorMessages = [];
    this.duties = [];
    this.planCom.scheduleDriverDuties(nd, cc, cm, pop, ng).subscribe(
      driverDuties => this.duties = driverDuties, 
      errorMessages => {
        console.log(errorMessages);
        errorMessages.error.forEach((s: any) => this.errorMessages.push(s.message));
      }
    );
  }

}
