import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriversSchedulerComponent } from './drivers-scheduler.component';

describe('DriversSchedulerComponent', () => {
  let component: DriversSchedulerComponent;
  let fixture: ComponentFixture<DriversSchedulerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriversSchedulerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
