import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { NodeInRoute } from 'src/app/models/planeamento/NodeInRoute';
import { PlanningService } from 'src/app/services/planeamento/Planning.service';
import { ChooseNodeComponent } from './choose-node/choose-node.component';

@Component({
  selector: 'app-shortest-path',
  templateUrl: './shortest-path.component.html',
  styleUrls: ['./shortest-path.component.css']
})
export class ShortestPathComponent implements OnInit {

  shortestPathForm: FormGroup;

  chosenFirstNodeCode: string;

  chosenSecondNodeCode: string;

  route: NodeInRoute[];

  errorMessages: string[];

  constructor(private translator: TranslateService,
              private builder: FormBuilder,
              private dialog: MatDialog,
              private planCom: PlanningService) 
  {
    this.shortestPathForm = this.builder.group({
      startHour: ''
    });
    this.route = [];
    this.errorMessages = [];
  }

  ngOnInit(): void {
  }

  onSubmit(formData: any) {
    const aux: string[] = formData.startHour.split(":");
    let time = (+aux[0]) * 60 * 60 + (+aux[1]) * 60;
    time = isNaN(time)? -1 : time;
    this.errorMessages = []
    this.planCom.findShortestRoute(this.chosenFirstNodeCode, this.chosenSecondNodeCode, time).subscribe(
      nodesOfRoute => this.route = nodesOfRoute, 
      errorMessages => {
        console.log(errorMessages);
        errorMessages.error.forEach((s: any) => this.errorMessages.push(s.message));
      }
    );
  }

  openChooseFirstNodeDialog(){
    const dialogRef = this.dialog.open(ChooseNodeComponent, {
      maxWidth: '820px',
      width: '90vw',
      height: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.chosenFirstNodeCode = result;
      }
    });
  }

  openChooseSecondNodeDialog(){
    const dialogRef = this.dialog.open(ChooseNodeComponent, {
      maxWidth: '1000px',
      width: '90vw',
      height: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.chosenSecondNodeCode = result;
      }
    });
  }

}
