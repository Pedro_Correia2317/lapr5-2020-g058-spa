
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NoDTO } from 'src/app/models/rede/no/NoDTO';

@Component({
  selector: 'app-choose-node',
  templateUrl: './choose-node.component.html',
  styleUrls: ['./choose-node.component.css']
})
export class ChooseNodeComponent implements OnInit {

  node: string;

  constructor(public dialogRef: MatDialogRef<ChooseNodeComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseNode(node: string){
      this.node = node;
  }

  onAddClick(){
      this.dialogRef.close(this.node);
  }

  onClickedNode(nodeDTO: NoDTO){
      this.chooseNode(nodeDTO.abreviatura);
  }

}
