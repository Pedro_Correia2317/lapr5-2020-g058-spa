import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { PlaneamentoComponent } from './planeamento.component';
import { TranslateModule } from '@ngx-translate/core';
import { ShortestPathComponent } from './shortest-path/shortest-path.component';
import { DriversSchedulerComponent } from './drivers-scheduler/drivers-scheduler.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VisualizadorNosModule } from '../rede/visualizadores/visualizador-nos/visualizador-nos.module';
import { ChooseNodeComponent } from './shortest-path/choose-node/choose-node.component';
import { MatDialogModule } from '@angular/material/dialog';

const routes: Route[] = [
  { path: '', redirectTo: 'shortest_route' },
  { path: '', component: PlaneamentoComponent, 
    children: [
      { path: 'shortest_route', component: ShortestPathComponent },
      { path: 'scheduling', component: DriversSchedulerComponent }
    ]
  }
]

@NgModule({
  declarations: [PlaneamentoComponent, ShortestPathComponent, DriversSchedulerComponent, ChooseNodeComponent],
  imports: [
    VisualizadorNosModule, CommonModule, MatDialogModule, FormsModule, ReactiveFormsModule, TranslateModule, RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  entryComponents: [ChooseNodeComponent]
})
export class PlaneamentoModule { }
