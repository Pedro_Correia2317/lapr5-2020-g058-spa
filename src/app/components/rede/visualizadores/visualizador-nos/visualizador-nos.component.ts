
import { Output } from '@angular/core';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { NoDTO } from 'src/app/models/rede/no/NoDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { AcessoNosService } from 'src/app/services/rede/AcessoNos.service';

@Component({
  selector: 'visualizador-nos',
  templateUrl: './visualizador-nos.component.html',
  styleUrls: ['./visualizador-nos.component.css']
})
export class VisualizadorNosComponent implements OnInit {

  sorts: string[] = ['DATA_DE_ADICAO', 'NOME', 'TIPO_NO', 'ABREVIATURA'];

  filters: string[] = ['NENHUM', 'NO_PARAGEM', 'NO_ESTACAO_RECOLHA', 'NO_PONTO_RENDICAO'];

  @Output() noClicado: EventEmitter<NoDTO> = new EventEmitter();

  nos: NoDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;
  
  isDescending: boolean;

  pesquisa: string;

  filtro: string;

  hasFinishedLoadingNodes: boolean;

  constructor(private todosNos: AcessoNosService) {
    this.nos = [];
    this.isDescending = false;
  }

  ngOnInit() {
    this.sort = this.sorts[0];
    this.filtro = this.filters[0];
  }

  onClickedNode(noSelecionado: NoDTO){
    this.noClicado.emit(noSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarNos(pagina, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarNos(1, this.tamanho, this.sort, pesquisa, this.isDescending, this.filtro);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarNos(1, tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarSort(sort: string){
    this.sort = sort;
    this.atualizarNos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarFiltro(filtro: string){
    this.filtro = filtro;
    this.atualizarNos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarOrdem(isDescending: boolean){
    this.isDescending = isDescending;
    this.atualizarNos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  private atualizarNos(pagina: number, tamanho: number, sort?: string, pesquisa?: string, isDescending?: boolean, filtro?: string){
    this.hasFinishedLoadingNodes = false;
    this.todosNos.procurarNos(pagina, tamanho, sort, pesquisa, isDescending, filtro).subscribe((resultados: DetalhesPesquisaDTO<NoDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.nos = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
        this.hasFinishedLoadingNodes = true;
      }
    });
  }

}