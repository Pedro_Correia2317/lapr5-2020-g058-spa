import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { TipoTripulanteDTO } from 'src/app/models/rede/tiposTripulantes/TipoTripulanteDTO';
import { AcessoTiposTripulantesService } from 'src/app/services/rede/AcessoTiposTripulantes.service';

@Component({
  selector: 'visualizador-tipos-tripulantes',
  templateUrl: './visualizador-tipos-tripulantes.component.html',
  styleUrls: ['./visualizador-tipos-tripulantes.component.css']
})
export class VisualizadorTiposTripulantesComponent implements OnInit {

  sorts: string[] = ['DATA_DE_ADICAO', 'CODIGO', 'DESCRICAO'];

  @Output() tipoTripulanteClicada: EventEmitter<TipoTripulanteDTO> = new EventEmitter();

  tiposTripulantes: TipoTripulanteDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;
  
  isDescending: boolean;

  pesquisa: string;

  hasFinishedLoadingDriverTypes: boolean;

  constructor(private todosTiposT: AcessoTiposTripulantesService) {
    this.tiposTripulantes = [];
  }

  ngOnInit() {
    this.sort = this.sorts[0];
  }

  onClickedDriverType(tipoTripulanteSelecionado: TipoTripulanteDTO){
    this.tipoTripulanteClicada.emit(tipoTripulanteSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarTiposTripulantes(pagina, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarTiposTripulantes(1, this.tamanho, this.sort, pesquisa, this.isDescending);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarTiposTripulantes(1, tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarSort(sort: string){
    this.sort = sort;
    this.atualizarTiposTripulantes(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarOrdem(isDescending: boolean){
    this.isDescending = isDescending;
    this.atualizarTiposTripulantes(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  private atualizarTiposTripulantes(pagina: number, tamanho: number, sort?: string, pesquisa?: string, isDescending?: boolean){
    this.hasFinishedLoadingDriverTypes = false;
    this.todosTiposT.procurarTiposTripulantes(pagina, tamanho, sort, pesquisa, isDescending).subscribe((resultados: DetalhesPesquisaDTO<TipoTripulanteDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.tiposTripulantes = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
        this.hasFinishedLoadingDriverTypes = true;
      }
    });
  }

}
