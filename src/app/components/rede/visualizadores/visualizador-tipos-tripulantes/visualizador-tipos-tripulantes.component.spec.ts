import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizadorTiposTripulantesComponent } from './visualizador-tipos-tripulantes.component';

describe('VisualizadorTiposTripulantesComponent', () => {
  let component: VisualizadorTiposTripulantesComponent;
  let fixture: ComponentFixture<VisualizadorTiposTripulantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizadorTiposTripulantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizadorTiposTripulantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
