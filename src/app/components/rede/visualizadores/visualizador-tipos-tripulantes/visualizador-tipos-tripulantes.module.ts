
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';

import { VisualizadorTiposTripulantesComponent } from './visualizador-tipos-tripulantes.component';

@NgModule({
  declarations: [VisualizadorTiposTripulantesComponent],
  imports: [VisualizacaoItemsModule, TranslateModule, CommonModule],
  exports: [VisualizadorTiposTripulantesComponent, RouterModule]
})
export class VisualizadorTiposTripulantesModule { }