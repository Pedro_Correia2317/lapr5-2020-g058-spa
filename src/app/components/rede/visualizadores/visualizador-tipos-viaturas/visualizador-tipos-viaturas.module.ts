
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';

import { VisualizadorTiposViaturasComponent } from './visualizador-tipos-viaturas.component';

@NgModule({
  declarations: [VisualizadorTiposViaturasComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [VisualizadorTiposViaturasComponent, RouterModule]
})
export class VisualizadorTiposViaturasModule { }