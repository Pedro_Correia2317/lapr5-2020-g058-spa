import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizadorTiposViaturasComponent } from './visualizador-tipos-viaturas.component';

describe('VisualizadorTiposViaturasComponent', () => {
  let component: VisualizadorTiposViaturasComponent;
  let fixture: ComponentFixture<VisualizadorTiposViaturasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizadorTiposViaturasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizadorTiposViaturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
