import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { TipoViaturaDTO } from 'src/app/models/rede/tipoViatura/TipoViaturaDTO';
import { AcessoTiposViaturasService } from 'src/app/services/rede/AcessoTiposViaturasService';

@Component({
  selector: 'visualizador-tipos-viaturas',
  templateUrl: './visualizador-tipos-viaturas.component.html',
  styleUrls: ['./visualizador-tipos-viaturas.component.css']
})
export class VisualizadorTiposViaturasComponent implements OnInit {

  sorts: string[] = ['DATA_DE_ADICAO', 'CODIGO', 'DESCRICAO', 'AUTONOMIA', 'VELOCIDADE', 'CUSTO', 'CONSUMO', 'EMISSOES', 'COMBUSTIVEL'];

  filters: string[] = ['NENHUM', 'DIESEL', 'GASOLINA', 'GPL', 'HIDROGENIO', 'ELECTRICO'];

  @Output() tipoViaturaClicada: EventEmitter<TipoViaturaDTO> = new EventEmitter();

  tiposViaturas: TipoViaturaDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;
  
  isDescending: boolean;

  pesquisa: string;

  filtro: string;

  hasFinishedLoadingVehicleTypes: boolean;

  constructor(private todosTiposV: AcessoTiposViaturasService) {
    this.tiposViaturas = [];
  }

  ngOnInit() {
    this.sort = this.sorts[0];
    this.filtro = this.filters[0];
  }

  onClickedVehicleType(tipoVeiculoSelecionado: TipoViaturaDTO){
    this.tipoViaturaClicada.emit(tipoVeiculoSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarTiposViaturas(pagina, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarTiposViaturas(1, this.tamanho, this.sort, pesquisa, this.isDescending, this.filtro);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarTiposViaturas(1, tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarSort(sort: string){
    this.sort = sort;
    this.atualizarTiposViaturas(1, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarFiltro(filtro: string){
    this.filtro = filtro;
    this.atualizarTiposViaturas(1, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  mudarOrdem(isDescending: boolean){
    this.isDescending = isDescending;
    this.atualizarTiposViaturas(1, this.tamanho, this.sort, this.pesquisa, this.isDescending, this.filtro);
  }

  private atualizarTiposViaturas(pagina: number, tamanho: number, sort?: string, pesquisa?: string, isDescending?: boolean, filtro?: string){
    this.hasFinishedLoadingVehicleTypes = false;
    this.todosTiposV.procurarTiposViaturas(pagina, tamanho, sort, pesquisa, isDescending, filtro).subscribe((resultados: DetalhesPesquisaDTO<TipoViaturaDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.tiposViaturas = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
        this.hasFinishedLoadingVehicleTypes = true;
      }
    });
  }

}
