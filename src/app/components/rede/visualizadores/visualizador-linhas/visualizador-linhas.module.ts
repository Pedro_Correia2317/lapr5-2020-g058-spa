
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';
import { VisualizadorLinhasComponent } from './visualizador-linhas.component';

@NgModule({
  declarations: [VisualizadorLinhasComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [VisualizadorLinhasComponent, RouterModule]
})
export class VisualizadorLinhasModule { }