import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LinhaDTO } from 'src/app/models/rede/linhas/LinhaDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { AcessoLinhasService } from 'src/app/services/rede/AcessoLinhas.service';

@Component({
  selector: 'visualizador-linhas',
  templateUrl: './visualizador-linhas.component.html',
  styleUrls: ['./visualizador-linhas.component.css']
})
export class VisualizadorLinhasComponent implements OnInit {

  sorts: string[] = ['DATA_DE_ADICAO', 'CODIGO', 'DESCRICAO'];

  @Output() linhaClicada: EventEmitter<LinhaDTO> = new EventEmitter();

  linhas: LinhaDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;
  
  isDescending: boolean;

  pesquisa: string;

  hasFinishedLoadingLines: boolean;

  constructor(private todasLinhas: AcessoLinhasService) {
    this.linhas = [];
  }

  ngOnInit() {
    this.sort = this.sorts[0];
  }

  onClickedLine(linhaSelecionada: LinhaDTO){
    this.linhaClicada.emit(linhaSelecionada);
  }

  mudarPagina(pagina: number){
    this.atualizarLinhas(pagina, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarLinhas(1, this.tamanho, this.sort, pesquisa, this.isDescending);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarLinhas(1, tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarSort(sort: string){
    this.sort = sort;
    this.atualizarLinhas(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarOrdem(isDescending: boolean){
    this.isDescending = isDescending;
    this.atualizarLinhas(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  private atualizarLinhas(pagina: number, tamanho: number, sort?: string, pesquisa?: string, isDescending?: boolean){
    this.hasFinishedLoadingLines = false;
    this.todasLinhas.procurarLinhas(pagina, tamanho, sort, pesquisa, isDescending).subscribe((resultados: DetalhesPesquisaDTO<LinhaDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.linhas = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
        this.hasFinishedLoadingLines = true;
      }
    });
  }

}
