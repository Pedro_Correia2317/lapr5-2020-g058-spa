import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizadorPercursosComponent } from './visualizador-percursos.component';

describe('VisualizadorPercursosComponent', () => {
  let component: VisualizadorPercursosComponent;
  let fixture: ComponentFixture<VisualizadorPercursosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizadorPercursosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizadorPercursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
