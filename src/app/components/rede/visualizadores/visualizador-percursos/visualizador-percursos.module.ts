
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';
import { VisualizadorPercursosComponent } from './visualizador-percursos.component';

@NgModule({
  declarations: [VisualizadorPercursosComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [VisualizadorPercursosComponent, RouterModule]
})
export class VisualizadorPercursosModule { }