import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DetalhesPesquisaDTO } from 'src/app/models/rede/pedidos/DetalhesPesquisaDTO';
import { PercursoDTO } from 'src/app/models/rede/percursos/PercursoDTO';
import { AcessoLinhasService } from 'src/app/services/rede/AcessoLinhas.service';
import { AcessoPercursosService } from 'src/app/services/rede/AcessoPercursos.service';

@Component({
  selector: 'visualizador-percursos',
  templateUrl: './visualizador-percursos.component.html',
  styleUrls: ['./visualizador-percursos.component.css']
})
export class VisualizadorPercursosComponent implements OnInit {

  sorts: string[] = ['DATA_DE_ADICAO', 'CODIGO', 'DESCRICAO', 'DISTANCIA_TOTAL', 'DURACAO_TOTAL'];

  @Input() line: string;

  @Output() percursoClicado: EventEmitter<PercursoDTO> = new EventEmitter();

  percursos: PercursoDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;
  
  isDescending: boolean;

  pesquisa: string;

  hasFinishedLoadingPaths: boolean;

  shouldShowOptionsBar: boolean;

  constructor(private todosPercs: AcessoPercursosService, private todasLinhas: AcessoLinhasService) {
    this.percursos = [];
    this.line = "";
    this.shouldShowOptionsBar = true;
  }

  ngOnInit() {
    this.sort = this.sorts[0];
  }

  onClickedPath(percursoSelecionado: PercursoDTO){
    this.percursoClicado.emit(percursoSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarPercursos(pagina, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarPercursos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarPercursos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarSort(sort: string){
    this.sort = sort;
    this.atualizarPercursos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  mudarOrdem(isDescending: boolean){
    this.isDescending = isDescending;
    this.atualizarPercursos(1, this.tamanho, this.sort, this.pesquisa, this.isDescending);
  }

  private atualizarPercursos(pagina: number, tamanho: number, sort?: string, pesquisa?: string, isDescending?: boolean){
    this.hasFinishedLoadingPaths = false;
    if(this.line != ""){
      this.todasLinhas.procurarPercursosLinha(this.line).subscribe((resultados: DetalhesPesquisaDTO<PercursoDTO[]>) => {
        if(!resultados.sucesso){
          alert(resultados.descricao);
        } else {
          this.percursos = [];
          resultados.lista.forEach(listaP => listaP.forEach(p => this.percursos.push(p)));
          this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
          this.hasFinishedLoadingPaths = true;
          this.shouldShowOptionsBar = false;
        }
      });
      return;
    }
    this.todosPercs.procurarPercursos(pagina, tamanho, sort, pesquisa, isDescending).subscribe((resultados: DetalhesPesquisaDTO<PercursoDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.percursos = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
        this.hasFinishedLoadingPaths = true;
      }
    });
  }

}
