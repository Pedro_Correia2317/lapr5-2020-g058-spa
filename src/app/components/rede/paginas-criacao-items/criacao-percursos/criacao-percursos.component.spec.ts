import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriacaoPercursosComponent } from './criacao-percursos.component';

describe('CriacaoPercursosComponent', () => {
  let component: CriacaoPercursosComponent;
  let fixture: ComponentFixture<CriacaoPercursosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriacaoPercursosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriacaoPercursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
