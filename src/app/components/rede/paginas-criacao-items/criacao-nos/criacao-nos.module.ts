
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizadorNosModule } from '../../visualizadores/visualizador-nos/visualizador-nos.module';

import { CriacaoNosComponent } from './criacao-nos.component';
import { CriacaoTemposComponent } from './criacao-tempos/criacao-tempos.component';

const routes: Routes = [
    {path: '', component: CriacaoNosComponent}
];

@NgModule({
  declarations: [CriacaoTemposComponent, CriacaoNosComponent],
  imports: [VisualizadorNosModule, TranslateModule, MatDialogModule, CommonModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [CriacaoTemposComponent]
})
export class CriacaoNosModule { }
