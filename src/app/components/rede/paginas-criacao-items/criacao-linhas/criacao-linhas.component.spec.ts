import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriacaoLinhasComponent } from './criacao-linhas.component';

describe('CriacaoLinhasComponent', () => {
  let component: CriacaoLinhasComponent;
  let fixture: ComponentFixture<CriacaoLinhasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriacaoLinhasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriacaoLinhasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
