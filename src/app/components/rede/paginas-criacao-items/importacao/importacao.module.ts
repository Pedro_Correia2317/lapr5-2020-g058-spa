
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ImportacaoComponent } from './importacao.component';

const routes: Routes = [
    {path: '', component: ImportacaoComponent}
];

@NgModule({
  declarations: [ImportacaoComponent],
  imports: [CommonModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportacaoModule { }