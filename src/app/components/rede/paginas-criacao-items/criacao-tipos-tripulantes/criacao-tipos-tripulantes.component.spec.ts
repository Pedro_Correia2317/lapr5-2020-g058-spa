import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriacaoTiposTripulantesComponent } from './criacao-tipos-tripulantes.component';

describe('CriacaoTiposTripulantesComponent', () => {
  let component: CriacaoTiposTripulantesComponent;
  let fixture: ComponentFixture<CriacaoTiposTripulantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriacaoTiposTripulantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriacaoTiposTripulantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
