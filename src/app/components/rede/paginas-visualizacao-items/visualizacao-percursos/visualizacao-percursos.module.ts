
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizadorPercursosModule } from '../../visualizadores/visualizador-percursos/visualizador-percursos.module';

import { VisualizacaoPercursosComponent } from './visualizacao-percursos.component';

const routes: Routes = [
    {path: '', component: VisualizacaoPercursosComponent}
];

@NgModule({
  declarations: [VisualizacaoPercursosComponent],
  imports: [CommonModule, TranslateModule, VisualizadorPercursosModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoPercursosModule { }