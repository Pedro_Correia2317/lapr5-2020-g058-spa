import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoPercursosComponent } from './visualizacao-percursos.component';

describe('VisualizacaoPercursosComponent', () => {
  let component: VisualizacaoPercursosComponent;
  let fixture: ComponentFixture<VisualizacaoPercursosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoPercursosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoPercursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
