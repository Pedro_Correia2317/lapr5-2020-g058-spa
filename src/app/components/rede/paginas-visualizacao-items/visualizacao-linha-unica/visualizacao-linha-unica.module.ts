import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizacaoLinhaUnicaComponent } from './visualizacao-linha-unica.component';
import { PercursoTemplateComponent } from './percurso-template/percurso-template.component';
import { TipoViaturaTemplateComponent } from './tipo-viatura-template/tipo-viatura-template.component';
import { TipoTripulanteTemplateComponent } from './tipo-tripulante-template/tipo-tripulante-template.component';

const routes: Routes = [
  {path: '', component: VisualizacaoLinhaUnicaComponent}
];

@NgModule({
declarations: [VisualizacaoLinhaUnicaComponent, PercursoTemplateComponent, TipoViaturaTemplateComponent, TipoTripulanteTemplateComponent],
imports: [CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class VisualizacaoLinhaUnicaModule { }
