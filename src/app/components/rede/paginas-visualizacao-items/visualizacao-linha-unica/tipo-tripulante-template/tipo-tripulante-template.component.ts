import { Component, Input, OnInit } from '@angular/core';
import { TipoTripulante } from 'src/app/models/rede/tiposTripulantes/TipoTripulante';

@Component({
  selector: 'app-tipo-tripulante-template',
  templateUrl: './tipo-tripulante-template.component.html',
  styleUrls: ['./tipo-tripulante-template.component.css']
})
export class TipoTripulanteTemplateComponent implements OnInit {

  @Input() tipoT: TipoTripulante;

  constructor() { }

  ngOnInit(): void {
  }

}
