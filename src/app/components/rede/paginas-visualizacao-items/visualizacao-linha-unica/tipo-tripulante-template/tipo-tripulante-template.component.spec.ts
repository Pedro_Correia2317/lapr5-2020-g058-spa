import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoTripulanteTemplateComponent } from './tipo-tripulante-template.component';

describe('TipoTripulanteTemplateComponent', () => {
  let component: TipoTripulanteTemplateComponent;
  let fixture: ComponentFixture<TipoTripulanteTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoTripulanteTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoTripulanteTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
