import { Component, Input, OnInit } from '@angular/core';
import { Percurso } from 'src/app/models/rede/percursos/Percurso';

@Component({
  selector: 'app-percurso-template',
  templateUrl: './percurso-template.component.html',
  styleUrls: ['./percurso-template.component.css']
})
export class PercursoTemplateComponent implements OnInit {

  @Input() perc: Percurso;

  constructor() { }

  ngOnInit(): void {
  }

}
