import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PercursoTemplateComponent } from './percurso-template.component';

describe('PercursoTemplateComponent', () => {
  let component: PercursoTemplateComponent;
  let fixture: ComponentFixture<PercursoTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PercursoTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PercursoTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
