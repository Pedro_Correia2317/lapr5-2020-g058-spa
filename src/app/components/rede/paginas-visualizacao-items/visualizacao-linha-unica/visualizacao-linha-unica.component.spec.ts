import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoLinhaUnicaComponent } from './visualizacao-linha-unica.component';

describe('VisualizacaoLinhaUnicaComponent', () => {
  let component: VisualizacaoLinhaUnicaComponent;
  let fixture: ComponentFixture<VisualizacaoLinhaUnicaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoLinhaUnicaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoLinhaUnicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
