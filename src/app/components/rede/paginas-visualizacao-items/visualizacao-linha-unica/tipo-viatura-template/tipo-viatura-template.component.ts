import { Component, Input, OnInit } from '@angular/core';
import { TipoViatura } from 'src/app/models/rede/tipoViatura/TipoViatura';

@Component({
  selector: 'app-tipo-viatura-template',
  templateUrl: './tipo-viatura-template.component.html',
  styleUrls: ['./tipo-viatura-template.component.css']
})
export class TipoViaturaTemplateComponent implements OnInit {

  @Input() tipoV: TipoViatura;

  constructor() { }

  ngOnInit(): void {
  }

}
