import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoViaturaTemplateComponent } from './tipo-viatura-template.component';

describe('TipoViaturaTemplateComponent', () => {
  let component: TipoViaturaTemplateComponent;
  let fixture: ComponentFixture<TipoViaturaTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoViaturaTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoViaturaTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
