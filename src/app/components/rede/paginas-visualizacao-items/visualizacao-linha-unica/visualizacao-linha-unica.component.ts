import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Linha } from 'src/app/models/rede/linhas/Linha';
import { Percurso } from 'src/app/models/rede/percursos/Percurso';
import { TipoTripulante } from 'src/app/models/rede/tiposTripulantes/TipoTripulante';
import { TipoViatura } from 'src/app/models/rede/tipoViatura/TipoViatura';
import { AcessoLinhasService } from 'src/app/services/rede/AcessoLinhas.service';
import { AcessoPercursosService } from 'src/app/services/rede/AcessoPercursos.service';
import { AcessoTiposTripulantesService } from 'src/app/services/rede/AcessoTiposTripulantes.service';
import { AcessoTiposViaturasService } from 'src/app/services/rede/AcessoTiposViaturasService';

@Component({
  selector: 'app-visualizacao-linha-unica',
  templateUrl: './visualizacao-linha-unica.component.html',
  styleUrls: ['./visualizacao-linha-unica.component.css']
})
export class VisualizacaoLinhaUnicaComponent implements OnInit {

  hasFinishedLoadingLine: boolean;

  hasFinishedLoadingGoPaths: boolean;

  hasFinishedLoadingReturnPaths: boolean;

  hasFinishedLoadingExtraPaths: boolean;

  hasFinishedLoadingEmptyPaths: boolean;

  hasFinishedLoadingVehicleTypes: boolean;

  hasFinishedLoadingDriverTypes: boolean;

  linha: Linha;

  percursosIda: Percurso[];

  percursosVolta: Percurso[];

  percursosReforco: Percurso[];

  percursosVazio: Percurso[];

  tiposViaturas: TipoViatura[];

  tiposTripulantes: TipoTripulante[];

  messages: string[];

  constructor(
    private todasLinhas: AcessoLinhasService, 
    private todosTiposViaturas: AcessoTiposViaturasService,
    private todosTiposTripulantes: AcessoTiposTripulantesService,
    private todosPercursos: AcessoPercursosService,
    private route: ActivatedRoute,
    private i18n: TranslateService)
  {
    this.messages = [];
    this.percursosIda = [];
    this.percursosVolta = [];
    this.percursosReforco = [];
    this.percursosVazio = [];
    this.tiposViaturas = [];
    this.tiposTripulantes = [];
    this.hasFinishedLoadingLine = false;
    this.hasFinishedLoadingGoPaths = false;
    this.hasFinishedLoadingReturnPaths = false;
    this.hasFinishedLoadingExtraPaths = false;
    this.hasFinishedLoadingEmptyPaths = false;
    this.hasFinishedLoadingVehicleTypes = false;
    this.hasFinishedLoadingDriverTypes = false;
    this.linha = new Linha();
  }

  ngOnInit(): void {
    const codigo = this.route.snapshot.paramMap.get('id');
    const aux = codigo == null? "" : codigo;
    this.todasLinhas.procurarLinha(aux).subscribe(
      (linha) => {
        this.linha = linha;
        this.procurarRestoDados();
        this.hasFinishedLoadingLine = true;
      },
      (error) => {
        this.messages = error.error;
        this.hasFinishedLoadingLine = true;
      }
    );
  }

  private procurarRestoDados(): void {
    this.procurarTiposViaturas();
    this.procurarTiposTripulantes();
    this.procurarPercursos(this.linha.percursosIda, this.percursosIda, () => this.hasFinishedLoadingGoPaths = true);
    this.procurarPercursos(this.linha.percursosVolta, this.percursosVolta, () => this.hasFinishedLoadingReturnPaths = true);
    this.procurarPercursos(this.linha.percursosReforco, this.percursosReforco, () => this.hasFinishedLoadingExtraPaths = true);
    this.procurarPercursos(this.linha.percursosVazios, this.percursosVazio, () => this.hasFinishedLoadingEmptyPaths = true);
  }

  private procurarTiposViaturas(): void {
    if(this.linha.tiposViaturas.length == 0){
      this.hasFinishedLoadingVehicleTypes = true;
    }
    this.linha.tiposViaturas.forEach((codTipo) => {
      this.todosTiposViaturas.procurarTipoViatura(codTipo).subscribe(
        (tipoViatura) => {
          tipoViatura.tipoCombustivel.descricao = this.i18n.instant(tipoViatura.tipoCombustivel.descricao);
          this.tiposViaturas.push(tipoViatura);
          this.hasFinishedLoadingVehicleTypes = true;
        },
        (error) => {
          this.messages.push(error.error);
          this.hasFinishedLoadingVehicleTypes = true;
        }
      )
    });
  }

  private procurarTiposTripulantes(): void {
    if(this.linha.tiposTripulantes.length == 0){
      this.hasFinishedLoadingDriverTypes = true;
    }
    this.linha.tiposTripulantes.forEach((codTipo) => {
      this.todosTiposTripulantes.procurarTipoTripulante(codTipo).subscribe(
        (tipoTripulante) => {
          this.tiposTripulantes.push(tipoTripulante);
          this.hasFinishedLoadingDriverTypes = true;
        },
        (error) => {
          this.messages.push(error.error);
          this.hasFinishedLoadingDriverTypes = true;
        }
      )
    });
  }

  private procurarPercursos(codigos: string[], lista: Percurso[], hasFinishedLoading: Function): void {
    if(codigos.length == 0){
      hasFinishedLoading();
    }
    codigos.forEach((codPerc) => {
      this.todosPercursos.procurarPercurso(codPerc).subscribe(
        (perc) => {
          lista.push(perc);
          hasFinishedLoading();
        },
        (error) => {
          this.messages.push(error.error);
          hasFinishedLoading();
        }
      )
    });
  }

}
