
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LinhaDTO } from 'src/app/models/rede/linhas/LinhaDTO';

@Component({
  selector: 'app-visualizacao-linhas',
  templateUrl: './visualizacao-linhas.component.html',
  styleUrls: ['./visualizacao-linhas.component.css']
})
export class VisualizacaoLinhasComponent implements OnInit {

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }

  onClickedLine(linha: LinhaDTO): void {
    this.router.navigate(['../linha', linha.codigo], {relativeTo: this.activatedRoute.parent});
  }

}
