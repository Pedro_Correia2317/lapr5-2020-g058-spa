import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoLinhasComponent } from './visualizacao-linhas.component';

describe('VisualizacaoLinhasComponent', () => {
  let component: VisualizacaoLinhasComponent;
  let fixture: ComponentFixture<VisualizacaoLinhasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoLinhasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoLinhasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
