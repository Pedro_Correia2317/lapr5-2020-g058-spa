import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoTiposTripulantesComponent } from './visualizacao-tipos-tripulantes.component';

describe('VisualizacaoTiposTripulantesComponent', () => {
  let component: VisualizacaoTiposTripulantesComponent;
  let fixture: ComponentFixture<VisualizacaoTiposTripulantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoTiposTripulantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoTiposTripulantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
