
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Configurations } from '../config/Configurations';
import { AuthGuardService } from '../services/login/AuthGuard.service';

const routes: Routes = [
  {
    path: 'rede', 
    loadChildren: () => import('./rede/rede.module').then(m => m.RedeModule),
    canActivate: [ AuthGuardService ],
    data: {roles: [Configurations.DATA_ADMINISTRATOR]}
  },
  {
    path: 'travel', 
    loadChildren: () => import('./viagem/viagem.module').then(m => m.ViagemModule),
    canActivate: [ AuthGuardService ],
    data: {roles: [Configurations.DATA_ADMINISTRATOR]}
  },
  {
    path: 'planning', 
    loadChildren: () => import('./planeamento/planeamento.module').then(m => m.PlaneamentoModule),
    canActivate: [ AuthGuardService ],
    data: {roles: [Configurations.MANAGER]}
  },
  {
    path: 'view', 
    loadChildren: () => import('./visualizacao-rede/visualizacao-rede.module').then(m => m.VisualizacaoRedeModule),
    canActivate: [ AuthGuardService ],
    data: {roles: [Configurations.CLIENT]}
  },
  {
    path: '', 
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
