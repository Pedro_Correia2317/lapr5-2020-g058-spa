import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Output() searchContent = new EventEmitter<string>();

  searchForm: any;

  constructor(private formBuilder: FormBuilder) {
    this.searchForm = this.formBuilder.group({ search: '' });
  }

  ngOnInit(): void {
  }

  ngOnSubmit(searchData: any){
    this.searchContent.emit(searchData.search); 
  }

}
