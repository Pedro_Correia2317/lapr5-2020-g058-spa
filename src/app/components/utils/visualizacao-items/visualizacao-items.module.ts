
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FiltersSelectorComponent } from './filters-selector/filters-selector.component';
import { PageSelectorComponent } from './page-selector/page-selector.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SizeSelectorComponent } from './size-selector/size-selector.component';
import { SortSelectorComponent } from './sort-selector/sort-selector.component';

@NgModule({
    declarations: [PageSelectorComponent, SearchBarComponent, SizeSelectorComponent, SortSelectorComponent, FiltersSelectorComponent],
    imports: [CommonModule, TranslateModule, FormsModule, ReactiveFormsModule],
    exports: [PageSelectorComponent, SearchBarComponent, SizeSelectorComponent, SortSelectorComponent, FiltersSelectorComponent, RouterModule]
  })
export class VisualizacaoItemsModule {}