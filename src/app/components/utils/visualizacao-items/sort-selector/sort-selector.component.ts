import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-sort-selector',
  templateUrl: './sort-selector.component.html',
  styleUrls: ['./sort-selector.component.css']
})
export class SortSelectorComponent implements OnInit, OnChanges {

  @Input() possibleSorts: string[];

  @Output() selectedValue = new EventEmitter<string>();

  @Output() selectedOrder = new EventEmitter<boolean>();

  chosenSort: string;

  isDescending: boolean;

  constructor() {
    this.isDescending = false;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.chosenSort = this.possibleSorts[0];
  }

  onSelection(event: any): void {
    this.selectedValue.emit(event.target.value);
  }

  changeOrder(): void {
    this.isDescending = !this.isDescending;
    this.selectedOrder.emit(this.isDescending);
  }

}
