import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-filters-selector',
  templateUrl: './filters-selector.component.html',
  styleUrls: ['./filters-selector.component.css']
})
export class FiltersSelectorComponent implements OnInit, OnChanges {

  @Input() possibleFilters: string[];

  @Output() selectedValue = new EventEmitter<string>();

  chosenFilter: string;

  constructor() {}

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.chosenFilter = this.possibleFilters[0];
  }

  onSelection(event: any): void {
    this.selectedValue.emit(event.target.value);
  }

}
