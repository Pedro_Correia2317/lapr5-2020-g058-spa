import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../services/login/Auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'VisualizacaoUI';

  isAuthenticated: boolean;

  role: string;
  
  constructor(private translate: TranslateService, 
              private authService: AuthService) {
      this.role = "";
      this.isAuthenticated = false;
      this.translate.setDefaultLang('en');
      this.authService.isAuthenticated.subscribe(
        (isAuthenticated: boolean)  => this.isAuthenticated = isAuthenticated
      );
      this.authService.role.subscribe(
        (role: string)  => this.role = role
      );

  }

  ngOnInit(): void {
    this.authService.checkAuthentication();
  }

  logout() {
    this.authService.logout('/');
  }

  changeLanguage(change: string){
    this.translate.use(change);
  }
}
