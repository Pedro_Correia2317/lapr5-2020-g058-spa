import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkBlocksCreationComponent } from './work-blocks-creation.component';

describe('WorkBlocksCreationComponent', () => {
  let component: WorkBlocksCreationComponent;
  let fixture: ComponentFixture<WorkBlocksCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkBlocksCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkBlocksCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
