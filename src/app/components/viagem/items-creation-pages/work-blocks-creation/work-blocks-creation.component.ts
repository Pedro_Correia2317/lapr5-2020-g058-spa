import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { CreateWorkBlockRequestDTO } from 'src/app/models/viagem/workblocks/CreateWorkBlockRequestDTO';
import { AccessWorkBlocksService } from 'src/app/services/viagem/AccesWorkBlock.service';
import { ChooseVehicleDutyComponent } from './choose-vehicle-duty/choose-vehicle-duty.component';

@Component({
  selector: 'app-work-blocks-creation',
  templateUrl: './work-blocks-creation.component.html',
  styleUrls: ['./work-blocks-creation.component.css']
})
export class WorkBlocksCreationComponent implements OnInit {

  createWorkBlockForm: FormGroup;

  vehicleDuties: string[]

  constructor(private allWorkBlocks: AccessWorkBlocksService, 
              private builder: FormBuilder,
              private dialog: MatDialog, 
              private translator: TranslateService) 
  {
    this.createWorkBlockForm = this.builder.group({maxBlockDuration: 0, maxNumberBlocks: 0});
    this.vehicleDuties = [];
  }

  ngOnInit(): void {
  }
  
  onSubmit(formData: any) {
    const dto = new CreateWorkBlockRequestDTO();
    dto.vehicleDuties = this.vehicleDuties;
    dto.maxBlockDuration = formData.maxBlockDuration;
    dto.numberMaxBlocks = formData.maxNumberBlocks;
    this.allWorkBlocks.sendWorkBlock(dto).subscribe(
      workBlock => alert(this.translator.instant("createWorkBlock.createSuccess")), 
      errorMessages => {
        let messages = "";
        errorMessages.error.forEach((s: string) => messages += this.translator.instant(s) + "\n");
        alert(messages);
      }
    );
  }

  openDialog(){
    const dialogRef = this.dialog.open(ChooseVehicleDutyComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.vehicleDuties.push(result);
      }
    });
  }

  eraseVehicleDuty(vehicleDuty: string){
    const index = this.vehicleDuties.indexOf(vehicleDuty, 0);
    if(index > -1){
      this.vehicleDuties.splice(index, 1);
    }
  }

}
