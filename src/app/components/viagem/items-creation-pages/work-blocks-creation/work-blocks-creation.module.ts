import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { ChooseVehicleDutyComponent } from './choose-vehicle-duty/choose-vehicle-duty.component';
import { WorkBlocksCreationComponent } from './work-blocks-creation.component';
import { VehicleDutiesViewerModule } from '../../viewers/vehicle-duties-viewer/vehicle-duties-viewer.module';

const routes: Routes = [
  {path: '', component: WorkBlocksCreationComponent}
];

@NgModule({
  declarations: [ WorkBlocksCreationComponent , ChooseVehicleDutyComponent],
  imports: [ VehicleDutiesViewerModule, CommonModule, MatDialogModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [ RouterModule ],
  entryComponents: [ ChooseVehicleDutyComponent ]
})
export class WorkBlocksCreationModule { }
