import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { VehicleDuty } from 'src/app/models/viagem/vehicleDuty/VehicleDuty';

@Component({
  selector: 'app-choose-vehicle-duty',
  templateUrl: './choose-vehicle-duty.component.html',
  styleUrls: ['./choose-vehicle-duty.component.css']
})
export class ChooseVehicleDutyComponent implements OnInit {

  vehicleDutyCode: string;

  constructor(public dialogRef: MatDialogRef<ChooseVehicleDutyComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseVehicleDuty(vehicleDutyCode: string){
      this.vehicleDutyCode = vehicleDutyCode;
  }

  onAddClick(){
      this.dialogRef.close(this.vehicleDutyCode);
  }

  onClickedVehicleDuty(vehicleDuty: VehicleDuty){
      this.chooseVehicleDuty(vehicleDuty.code);
  }

}
