import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseVehicleDutyComponent } from './choose-vehicle-duty.component';

describe('ChooseVehicleDutyComponent', () => {
  let component: ChooseVehicleDutyComponent;
  let fixture: ComponentFixture<ChooseVehicleDutyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseVehicleDutyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseVehicleDutyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
