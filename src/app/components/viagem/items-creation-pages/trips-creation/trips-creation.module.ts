import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { TripsCreationComponent } from './trips-creation.component';
import { ChooseLineComponent } from './choose-line/choose-line.component';
import { ChoosePathComponent } from './choose-path/choose-path.component';
import { VisualizadorPercursosModule } from 'src/app/components/rede/visualizadores/visualizador-percursos/visualizador-percursos.module';
import { VisualizadorLinhasModule } from 'src/app/components/rede/visualizadores/visualizador-linhas/visualizador-linhas.module';

const routes: Routes = [
  {path: '', component: TripsCreationComponent}
];

@NgModule({
  declarations: [ TripsCreationComponent , ChooseLineComponent, ChoosePathComponent],
  imports: [ VisualizadorPercursosModule, VisualizadorLinhasModule, CommonModule, MatDialogModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [ RouterModule ],
  entryComponents: [ ChooseLineComponent, ChoosePathComponent ]
})
export class TripsCreationModule { }
