import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PercursoDTO } from 'src/app/models/rede/percursos/PercursoDTO';

@Component({
  selector: 'app-choose-path',
  templateUrl: './choose-path.component.html',
  styleUrls: ['./choose-path.component.css']
})
export class ChoosePathComponent implements OnInit {

  line: string;

  pathCode: string;

  constructor(@Inject(MAT_DIALOG_DATA) public passedLine: string, public dialogRef: MatDialogRef<ChoosePathComponent>) {
    this.line = passedLine;
  }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  choosePath(pathCode: string){
      this.pathCode = pathCode;
  }

  onAddClick(){
      this.dialogRef.close(this.pathCode);
  }

  onClickedPath(pathDTO: PercursoDTO){
      this.choosePath(pathDTO.codigo);
  }

}
