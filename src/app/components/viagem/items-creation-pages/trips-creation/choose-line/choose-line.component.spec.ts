import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseLineComponent } from './choose-line.component';

describe('ChooseLineComponent', () => {
  let component: ChooseLineComponent;
  let fixture: ComponentFixture<ChooseLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseLineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
