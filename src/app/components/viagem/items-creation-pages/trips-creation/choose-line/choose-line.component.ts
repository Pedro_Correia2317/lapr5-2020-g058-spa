import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { LinhaDTO } from 'src/app/models/rede/linhas/LinhaDTO';

@Component({
  selector: 'app-choose-line',
  templateUrl: './choose-line.component.html',
  styleUrls: ['./choose-line.component.css']
})
export class ChooseLineComponent implements OnInit {

  lineCode: string;

  constructor(public dialogRef: MatDialogRef<ChooseLineComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseLine(lineCode: string){
      this.lineCode = lineCode;
  }

  onAddClick(){
      this.dialogRef.close(this.lineCode);
  }

  onClickedLine(lineDTO: LinhaDTO){
      this.chooseLine(lineDTO.codigo);
  }

}
