import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { CreateTripRequestDTO } from 'src/app/models/viagem/trip/CreateTripRequestDTO';
import { AccessTripsService } from 'src/app/services/viagem/AccessTrips.service';
import { ChooseLineComponent } from './choose-line/choose-line.component';
import { ChoosePathComponent } from './choose-path/choose-path.component';

@Component({
  selector: 'app-Trips-creation',
  templateUrl: './trips-creation.component.html',
  styleUrls: ['./trips-creation.component.css']
})
export class TripsCreationComponent implements OnInit {

  createTripForm: FormGroup;

  chosenLineCode: string;

  chosenPathCode: string;

  constructor(private translator: TranslateService,
              private builder: FormBuilder,
              private dialog: MatDialog,
              private allTrips: AccessTripsService) 
  {
    this.createTripForm = this.builder.group({startHour: ''});
  }

  ngOnInit(): void {
  }

  onSubmit(formData: any) {
    const dto = new CreateTripRequestDTO();
    dto.startHour = formData.startHour;
    dto.line = this.chosenLineCode;
    dto.path = this.chosenPathCode;
    this.allTrips.sendTrip(dto).subscribe(
      Trip => alert(this.translator.instant("createTrip.createSuccess")), 
      errorMessages => {
        let messages = "";
        errorMessages.error.forEach((s: string) => messages += this.translator.instant(s) + "\n");
        alert(messages);
      }
      );
  }

  openChooseLineDialog(){
    const dialogRef = this.dialog.open(ChooseLineComponent, {
      maxWidth: '820px',
      width: '90vw',
      height: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.chosenLineCode = result;
      }
    });
  }

  openChoosePathDialog(){
    const dialogRef = this.dialog.open(ChoosePathComponent, {
      maxWidth: '820px',
      width: '90vw',
      height: '90vh',
      data: this.chosenLineCode
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.chosenPathCode = result;
      }
    });
  }

}
