import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsCreationComponent } from './trips-creation.component';

describe('TripsCreationComponent', () => {
  let component: TripsCreationComponent;
  let fixture: ComponentFixture<TripsCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripsCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
