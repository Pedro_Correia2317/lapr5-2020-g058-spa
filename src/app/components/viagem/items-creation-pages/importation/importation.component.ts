import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CreateDriverDutyRequestDTO } from 'src/app/models/viagem/driverDuty/CreateDriverDutyRequestDTO';
import { ImportationDetailsDTO } from 'src/app/models/viagem/importation/ImportationDetailsDTO';
import { ReadingErrorDTO } from 'src/app/models/viagem/importation/ReadingErrorDTO';
import { CreateTripRequestDTO } from 'src/app/models/viagem/trip/CreateTripRequestDTO';
import { CreateVehicleDutyRequestDTO } from 'src/app/models/viagem/vehicleDuty/CreateVehicleDutyRequestDTO';
import { CreateWorkBlockRequestDTO } from 'src/app/models/viagem/workblocks/CreateWorkBlockRequestDTO';
import { AcessoImportacaoService } from 'src/app/services/rede/AcessoImportacao.service';
import { AccessDriverDutiesService } from 'src/app/services/viagem/AccessDriverDuties.service';
import { AccessTripsService } from 'src/app/services/viagem/AccessTrips.service';
import { AccessVehicleDutiesService } from 'src/app/services/viagem/AccessVehicleDuties.service';
import { AccessWorkBlocksService } from 'src/app/services/viagem/AccesWorkBlock.service';

@Component({
  selector: 'app-importation',
  templateUrl: './importation.component.html',
  styleUrls: ['./importation.component.css']
})
export class ImportationComponent implements OnInit {

  private file: File;

  parsingErrors: ReadingErrorDTO[];

  trips: CreateTripRequestDTO[];

  vehicleDuties: CreateVehicleDutyRequestDTO[];

  workBlocks: CreateWorkBlockRequestDTO[];

  driverDuties: CreateDriverDutyRequestDTO[];

  processingErrors: string[];

  selectedTripsIndexes: Map<number, boolean>;

  selectedVehicleDutiesIndexes: Map<number, boolean>;

  selectedWorkBlocksIndexes: Map<number, boolean>;

  selectedDriverDutiesIndexes: Map<number, boolean>;

  isAllTripsSelected: boolean;

  isAllVehicleDutiesSelected: boolean;

  isAllWorkBlocksSelected: boolean;

  isAllDriverDutiesSelected: boolean;

  isFileSubmitted: boolean;

  tripsMessages: string[];

  vehicleDutiesMessages: string[];

  workBlockMessages: string[];

  driverDutiesMessages: string[];

  constructor(
    private allTrips: AccessTripsService,
    private allVehicleDuties: AccessVehicleDutiesService,
    private allWorkBlocks: AccessWorkBlocksService,
    private allDriverDuties: AccessDriverDutiesService,
    private importation: AcessoImportacaoService,
    private translator: TranslateService
  ) {
    this.cleanArrays();
    this.isAllTripsSelected = false;
    this.isAllVehicleDutiesSelected = false;
    this.isAllWorkBlocksSelected = false;
    this.isAllDriverDutiesSelected = false;
    this.isFileSubmitted = false;
  }

  private cleanArrays()
  {
    this.parsingErrors = [];
    this.trips = [];
    this.vehicleDuties = [];
    this.workBlocks = [];
    this.driverDuties = [];
    this.processingErrors = [];
    this.tripsMessages = [];
    this.vehicleDutiesMessages = [];
    this.workBlockMessages = [];
    this.driverDutiesMessages = [];
    this.selectedTripsIndexes = new Map();
    this.selectedVehicleDutiesIndexes = new Map();
    this.selectedWorkBlocksIndexes = new Map();
    this.selectedDriverDutiesIndexes = new Map();
  }

  ngOnInit(): void {
  }

  onFileChange(event: any){
    this.file = event.target.files[0];
  }

  onSubmit() {
    this.cleanArrays();
    this.importation.sendTravelFile(this.file).subscribe(
      (results: ImportationDetailsDTO) => { 
        this.parsingErrors = results.errors; 
        this.trips = results.trips; 
        this.vehicleDuties = results.vehicleDuties; 
        this.workBlocks = results.workBlocks; 
        this.driverDuties = results.driverDuties;
        this.isFileSubmitted = true;
      },
      error => { this.processingErrors = error.error; }
    );
  }

  isTripSelected(index: number) : boolean {
    const operation: boolean | undefined = this.selectedTripsIndexes.get(index);
    if (operation == null) {
      return false;
    }
    return operation;
  }

  isVehicleDutySelected(index: number) : boolean {
    const operation: boolean | undefined = this.selectedVehicleDutiesIndexes.get(index);
    if (operation == null) {
      return false;
    }
    return operation;
  }

  isWorkBlockSelected(index: number) : boolean {
    const operation: boolean | undefined = this.selectedWorkBlocksIndexes.get(index);
    if (operation == null) {
      return false;
    }
    return operation;
  }

  isDriverDutySelected(index: number) : boolean {
    const operation: boolean | undefined = this.selectedDriverDutiesIndexes.get(index);
    if (operation == null) {
      return false;
    }
    return operation;
  }

  onSelectedTrip(index: number) {
    let operation: boolean | undefined = this.selectedTripsIndexes.get(index);
    operation = operation == undefined? true : !operation;
    this.selectedTripsIndexes.set(index, operation);
  }

  onSelectedVehicleDuty(index: number) {
    let operation: boolean | undefined = this.selectedVehicleDutiesIndexes.get(index);
    operation = operation == undefined? true : !operation;
    this.selectedVehicleDutiesIndexes.set(index, operation);
  }

  onSelectedWorkBlock(index: number) {
    let operation: boolean | undefined = this.selectedWorkBlocksIndexes.get(index);
    operation = operation == undefined? true : !operation;
    this.selectedWorkBlocksIndexes.set(index, operation);
  }

  onSelectedDriverDuty(index: number) {
    let operation: boolean | undefined = this.selectedDriverDutiesIndexes.get(index);
    operation = operation == undefined? true : !operation;
    this.selectedDriverDutiesIndexes.set(index, operation);
  }

  selectAllTrips(): void {
    this.isAllTripsSelected = !this.isAllTripsSelected;
    for (let i = 0; i < this.trips.length; i++) {
      this.selectedTripsIndexes.set(i, this.isAllTripsSelected);
    }
  }

  selectAllVehicleDuties(): void {
    this.isAllVehicleDutiesSelected = !this.isAllVehicleDutiesSelected;
    for (let i = 0; i < this.vehicleDuties.length; i++) {
      this.selectedVehicleDutiesIndexes.set(i, this.isAllVehicleDutiesSelected);
    }
  }

  selectAllWorkBlocks(): void {
    this.isAllWorkBlocksSelected = !this.isAllWorkBlocksSelected;
    for (let i = 0; i < this.workBlocks.length; i++) {
      this.selectedWorkBlocksIndexes.set(i, this.isAllWorkBlocksSelected);
    }
  }

  selectAllDriverDuties(): void {
    this.isAllDriverDutiesSelected = !this.isAllDriverDutiesSelected;
    for (let i = 0; i < this.driverDuties.length; i++) {
      this.selectedDriverDutiesIndexes.set(i, this.isAllDriverDutiesSelected);
    }
  }

  onTravelsSubmit() {
    this.selectedTripsIndexes.forEach((v, k) => {
      v && this.allTrips.sendTrip(this.trips[k]).subscribe(
        success => this.tripsMessages.push(this.trips[k].code + " - " + this.translator.instant("createTrip.createSuccess")),
        error => error.error.forEach((err: string) => this.tripsMessages.push(this.trips[k].code +  " - " + this.translator.instant(err)))
      );
    });
    this.selectedVehicleDutiesIndexes.forEach((v, k) => {
      v && this.allVehicleDuties.sendVehicleDuty(this.vehicleDuties[k]).subscribe(
        success => this.vehicleDutiesMessages.push(this.vehicleDuties[k].code + " - " + this.translator.instant("createVehicleDuty.createSuccess")),
        error => error.error.forEach((err: string) => this.vehicleDutiesMessages.push(this.vehicleDuties[k].code +  " - " + this.translator.instant(err)))
      );
    });
    this.selectedWorkBlocksIndexes.forEach((v, k) => {
      const wb = this.workBlocks[k];
      v && this.allWorkBlocks.sendWorkBlock(wb).subscribe(
        success => success.forEach(wb =>
          this.workBlockMessages.push(wb.code +  " - " + this.translator.instant("createWorkBlock.createSuccess"))
        ),
        error => error.error.forEach((err: string) => this.workBlockMessages.push(wb.codes[0] + " -> " + wb.codes[wb.codes.length-1] +  " - " + this.translator.instant(err)))
        );
    });
    this.selectedDriverDutiesIndexes.forEach((v, k) => {
      v && this.allDriverDuties.sendDriverDuty(this.driverDuties[k]).subscribe(
        success => this.driverDutiesMessages.push(this.driverDuties[k].code + " - " + this.translator.instant("createDriverDuty.createSuccess")),
        error => this.driverDutiesMessages.push(this.driverDuties[k].code +  " - " + this.translator.instant(error.error[0]))
      );
    });
  }

}
