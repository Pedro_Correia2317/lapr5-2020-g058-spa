import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportationComponent } from './importation.component';

const routes: Routes = [
  {path: '', component: ImportationComponent}
];

@NgModule({
  declarations: [ ImportationComponent ],
  imports: [ CommonModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [ RouterModule ]
})
export class ImportationModule { }