import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Trip } from 'src/app/models/viagem/trip/Trip';

@Component({
  selector: 'app-choose-trip',
  templateUrl: './choose-trip.component.html',
  styleUrls: ['./choose-trip.component.css']
})
export class ChooseTripComponent implements OnInit {

  tripCode: string;

  constructor(public dialogRef: MatDialogRef<ChooseTripComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseTrip(tripCode: string){
      this.tripCode = tripCode;
  }

  onAddClick(){
      this.dialogRef.close(this.tripCode);
  }

  onClickedTrip(trip: Trip){
      this.chooseTrip(trip.code);
  }

}
