import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDutiesCreationComponent } from './vehicle-duties-creation.component';

describe('VehicleDutiesCreationComponent', () => {
  let component: VehicleDutiesCreationComponent;
  let fixture: ComponentFixture<VehicleDutiesCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleDutiesCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDutiesCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
