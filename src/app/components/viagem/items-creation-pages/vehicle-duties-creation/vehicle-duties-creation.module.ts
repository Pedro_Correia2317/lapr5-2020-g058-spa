import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { VehicleDutiesCreationComponent } from './vehicle-duties-creation.component';
import { ChooseTripComponent } from './choose-trip/choose-trip.component';
import { TripsViewerModule } from '../../viewers/trips-viewers/trips-viewers.module';

const routes: Routes = [
  {path: '', component: VehicleDutiesCreationComponent}
];

@NgModule({
  declarations: [ VehicleDutiesCreationComponent , ChooseTripComponent],
  imports: [ TripsViewerModule, CommonModule, MatDialogModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [ RouterModule ],
  entryComponents: [ ChooseTripComponent ]
})
export class VehicleDutiesCreationModule { }
