import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { CreateVehicleDutyRequestDTO } from 'src/app/models/viagem/vehicleDuty/CreateVehicleDutyRequestDTO';
import { AccessVehicleDutiesService } from 'src/app/services/viagem/AccessVehicleDuties.service';
import { ChooseTripComponent } from './choose-trip/choose-trip.component';

@Component({
  selector: 'app-vehicle-duties-creation',
  templateUrl: './vehicle-duties-creation.component.html',
  styleUrls: ['./vehicle-duties-creation.component.css']
})
export class VehicleDutiesCreationComponent implements OnInit {

  trips: string[]

  constructor(private allVehicleDuties: AccessVehicleDutiesService, 
              private dialog: MatDialog, 
              private translator: TranslateService) {
    this.trips = [];
  }

  ngOnInit(): void {
  }
  
  onSubmit() {
    const dto = new CreateVehicleDutyRequestDTO();
    dto.trips = this.trips;
    this.allVehicleDuties.sendVehicleDuty(dto).subscribe(
      vehicle => alert(this.translator.instant("createVehicleDuty.createSuccess")), 
      errorMessages => {
        let messages = "";
        errorMessages.error.forEach((s: string) => messages += this.translator.instant(s) + "\n");
        alert(messages);
      }
    );
  }

  openDialog(){
    const dialogRef = this.dialog.open(ChooseTripComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.trips.push(result);
      }
    });
  }

  eraseTrip(trip: string){
    const index = this.trips.indexOf(trip, 0);
    if(index > -1){
      this.trips.splice(index, 1);
    }
  }

}
