import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TipoTripulanteDTO } from 'src/app/models/rede/tiposTripulantes/TipoTripulanteDTO';

@Component({
  selector: 'app-choose-driver-type',
  templateUrl: './choose-driver-type.component.html',
  styleUrls: ['./choose-driver-type.component.css']
})
export class ChooseDriverTypeComponent implements OnInit {

  driverType: string;

  constructor(public dialogRef: MatDialogRef<ChooseDriverTypeComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseDriverType(driverType: string){
      this.driverType = driverType;
  }

  onAddClick(){
      this.dialogRef.close(this.driverType);
  }

  onClickedDriverType(driverTypeDTO: TipoTripulanteDTO){
      this.chooseDriverType(driverTypeDTO.codigo);
  }

}
