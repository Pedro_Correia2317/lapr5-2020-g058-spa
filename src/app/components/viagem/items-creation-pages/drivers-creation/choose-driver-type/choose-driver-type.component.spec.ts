import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseDriverTypeComponent } from './choose-driver-type.component';

describe('ChooseDriverTypeComponent', () => {
  let component: ChooseDriverTypeComponent;
  let fixture: ComponentFixture<ChooseDriverTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseDriverTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseDriverTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
