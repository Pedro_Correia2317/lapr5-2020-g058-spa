import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";
import { CreateDriverRequestDTO } from "src/app/models/viagem/driver/CreateDriverRequestDTO";
import { AccessDriversService } from "src/app/services/viagem/AccessDrivers.service";
import { ChooseDriverTypeComponent } from "./choose-driver-type/choose-driver-type.component";

@Component({
    selector: 'app-drivers-creation',
    templateUrl: './drivers-creation.component.html',
    styleUrls: ['./drivers-creation.component.css']
})
export class DriversCreationComponent implements OnInit {

    createDriverForm: FormGroup;

    chosenTypeCodes: string[];

    constructor(private translator: TranslateService,
        private builder: FormBuilder,
        private dialog: MatDialog,
        private allDrivers: AccessDriversService) {
        this.createDriverForm = this.builder.group({
            mecanographicNumber: '',
            name: '',
            birthDate: '',
            ccNumb: '',
            nif: '',
            initialContractDate: '',
            endContractDate: ''
        });
        this.chosenTypeCodes = []
    }

    ngOnInit(): void {
    }

    onSubmit(formData: any) {
        const dto = new CreateDriverRequestDTO();
        dto.mecNumber = formData.mecanographicNumber;
        dto.driverName = formData.name;
        const aux: string[] = formData.birthDate.split("-");
        dto.birthDate = aux[2] + "-" + aux[1] + "-" + aux[0];
        dto.cCNumb = formData.ccNumb;
        dto.nIF = formData.nif;
        const aux1: string[] = formData.initialContractDate.split("-");
        dto.initDate = aux1[2] + "-" + aux1[1] + "-" + aux1[0];
        const aux2: string[] = formData.endContractDate.split("-");
        dto.endDate = aux2[2] + "-" + aux2[1] + "-" + aux2[0];
        dto.listDriverTypeCode = this.chosenTypeCodes;
        this.allDrivers.sendVehicle(dto).subscribe(
            driver => alert(this.translator.instant("createDriver.createSuccess")),
            errorMessages => {
                let messages = "";
                errorMessages.error.forEach((s: string) => messages += this.translator.instant(s) + "\n");
                alert(messages);
            }
        );
    }

    openDialog() {
        const dialogRef = this.dialog.open(ChooseDriverTypeComponent, {
            maxWidth: '1000px',
            width: '90vw',
            height: '90vh'
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result != null) {
                this.chosenTypeCodes.push(result);
            }
        });
    }

    eraseDriverType(driverType: string){
        const index = this.chosenTypeCodes.indexOf(driverType, 0);
        if(index > -1){
          this.chosenTypeCodes.splice(index, 1);
        }
    }

}