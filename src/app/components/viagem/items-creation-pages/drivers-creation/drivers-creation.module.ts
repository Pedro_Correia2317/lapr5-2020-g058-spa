import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogModule } from "@angular/material/dialog";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { VisualizadorTiposTripulantesModule } from "src/app/components/rede/visualizadores/visualizador-tipos-tripulantes/visualizador-tipos-tripulantes.module";
import { ChooseDriverTypeComponent } from "./choose-driver-type/choose-driver-type.component";
import { DriversCreationComponent } from "./drivers-creation.component";

const routes: Routes = [
    {path: '', component: DriversCreationComponent}
  ];
  
  @NgModule({
    declarations: [ DriversCreationComponent, ChooseDriverTypeComponent ],
    imports: [ VisualizadorTiposTripulantesModule, CommonModule, MatDialogModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
    exports: [ RouterModule ],
    entryComponents: [ChooseDriverTypeComponent]
  })
  export class DriversCreationModule { }