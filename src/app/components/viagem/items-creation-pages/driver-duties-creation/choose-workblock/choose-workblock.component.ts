import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { WorkBlock } from 'src/app/models/viagem/workblocks/WorkBlock';

@Component({
  selector: 'app-choose-workblock',
  templateUrl: './choose-workblock.component.html',
  styleUrls: ['./choose-workblock.component.css']
})
export class ChooseWorkblockComponent implements OnInit {

  workBlockCode: string;

  constructor(public dialogRef: MatDialogRef<ChooseWorkblockComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseWorkBlock(workBlockCode: string){
      this.workBlockCode = workBlockCode;
  }

  onAddClick(){
      this.dialogRef.close(this.workBlockCode);
  }

  onClickedWorkBlock(workBlock: WorkBlock){
      this.chooseWorkBlock(workBlock.code);
  }

}
