import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseWorkblockComponent } from './choose-workblock.component';

describe('ChooseWorkblockComponent', () => {
  let component: ChooseWorkblockComponent;
  let fixture: ComponentFixture<ChooseWorkblockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseWorkblockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseWorkblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
