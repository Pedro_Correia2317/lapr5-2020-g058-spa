import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateDriverDutyRequestDTO } from 'src/app/models/viagem/driverDuty/CreateDriverDutyRequestDTO';
import { AccessDriverDutiesService } from 'src/app/services/viagem/AccessDriverDuties.service';
import { ChooseWorkblockComponent } from './choose-workblock/choose-workblock.component';

@Component({
  selector: 'app-driver-duties-creation',
  templateUrl: './driver-duties-creation.component.html',
  styleUrls: ['./driver-duties-creation.component.css']
})
export class DriverDutiesCreationComponent implements OnInit {

  workBlocks: string[]

  messages: string[];

  constructor(private allDriverDuties: AccessDriverDutiesService, 
              private dialog: MatDialog) {
    this.workBlocks = [];
    this.messages = [];
  }

  ngOnInit(): void {
  }
  
  onSubmit() {
    const dto = new CreateDriverDutyRequestDTO();
    dto.workBlocks = this.workBlocks;
    this.messages = [];
    this.allDriverDuties.sendDriverDuty(dto).subscribe(
      driverDuty => this.messages.push("createDriverDuty.createSuccess"), 
      error => this.messages = error.error
    );
  }

  openDialog(){
    const dialogRef = this.dialog.open(ChooseWorkblockComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.workBlocks.push(result);
      }
    });
  }

  eraseWorkBlock(index: number){
    this.workBlocks.splice(index, 1);
  }

}
