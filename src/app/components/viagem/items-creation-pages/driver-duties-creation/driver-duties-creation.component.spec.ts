import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverDutiesCreationComponent } from './driver-duties-creation.component';

describe('DriverDutiesCreationComponent', () => {
  let component: DriverDutiesCreationComponent;
  let fixture: ComponentFixture<DriverDutiesCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverDutiesCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverDutiesCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
