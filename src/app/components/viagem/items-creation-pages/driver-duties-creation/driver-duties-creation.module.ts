import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { DriverDutiesCreationComponent } from './driver-duties-creation.component';
import { ChooseWorkblockComponent } from './choose-workblock/choose-workblock.component';
import { WorkBlocksViewerModule } from '../../viewers/work-blocks-viewer/work-blocks-viewer.module';

const routes: Routes = [
  {path: '', component: DriverDutiesCreationComponent}
];

@NgModule({
  declarations: [ DriverDutiesCreationComponent , ChooseWorkblockComponent],
  imports: [ WorkBlocksViewerModule, CommonModule, MatDialogModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [ RouterModule ],
  entryComponents: [ ChooseWorkblockComponent ]
})
export class DriverDutiesCreationModule { }