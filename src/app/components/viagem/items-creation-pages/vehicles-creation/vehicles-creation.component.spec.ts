import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesCreationComponent } from './vehicles-creation.component';

describe('VehiclesCreationComponent', () => {
  let component: VehiclesCreationComponent;
  let fixture: ComponentFixture<VehiclesCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiclesCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
