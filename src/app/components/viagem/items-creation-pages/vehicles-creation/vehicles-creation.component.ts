import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { CreateVehicleRequestDTO } from 'src/app/models/viagem/vehicle/CreateVehicleRequestDTO';
import { AccessVehiclesService } from 'src/app/services/viagem/AccessVehicles.service';
import { ChooseVehicleTypeComponent } from './choose-vehicle-type/choose-vehicle-type.component';

@Component({
  selector: 'app-vehicles-creation',
  templateUrl: './vehicles-creation.component.html',
  styleUrls: ['./vehicles-creation.component.css']
})
export class VehiclesCreationComponent implements OnInit {

  createVehicleForm: FormGroup;

  chosenTypeCode: string;

  constructor(private translator: TranslateService,
              private builder: FormBuilder,
              private dialog: MatDialog,
              private allVehicles: AccessVehiclesService) 
  {
    this.createVehicleForm = this.builder.group({
      licensePlate: '',
      vin: '',
      entryDate: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(formData: any) {
    const dto = new CreateVehicleRequestDTO();
    dto.licensePlate = formData.licensePlate;
    dto.vin = formData.vin;
    const aux: string[] = formData.entryDate.split("-");
    dto.entryDate = aux[2] + "-" + aux[1] + "-" + aux[0];
    dto.vehicleType = this.chosenTypeCode;
    this.allVehicles.sendVehicle(dto).subscribe(
      vehicle => alert(this.translator.instant("createVehicle.createSuccess")), 
      errorMessages => {
        let messages = "";
        errorMessages.error.forEach((s: string) => messages += this.translator.instant(s) + "\n");
        alert(messages);
      }
      );
  }

  openDialog(){
    const dialogRef = this.dialog.open(ChooseVehicleTypeComponent, {
      maxWidth: '820px',
      width: '90vw',
      height: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.chosenTypeCode = result;
      }
    });
  }

}
