import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesCreationComponent } from './vehicles-creation.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChooseVehicleTypeComponent } from './choose-vehicle-type/choose-vehicle-type.component';
import { VisualizadorTiposViaturasModule } from 'src/app/components/rede/visualizadores/visualizador-tipos-viaturas/visualizador-tipos-viaturas.module';
import { MatDialogModule } from '@angular/material/dialog';

const routes: Routes = [
  {path: '', component: VehiclesCreationComponent}
];

@NgModule({
  declarations: [ VehiclesCreationComponent, ChooseVehicleTypeComponent ],
  imports: [ VisualizadorTiposViaturasModule, CommonModule, MatDialogModule, TranslateModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [ RouterModule ],
  entryComponents: [ChooseVehicleTypeComponent]
})
export class VehiclesCreationModule { }
