import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TipoViaturaDTO } from 'src/app/models/rede/tipoViatura/TipoViaturaDTO';

@Component({
  selector: 'app-choose-vehicle-type',
  templateUrl: './choose-vehicle-type.component.html',
  styleUrls: ['./choose-vehicle-type.component.css']
})
export class ChooseVehicleTypeComponent implements OnInit {

  vehicleType: string;

  constructor(public dialogRef: MatDialogRef<ChooseVehicleTypeComponent>) { }

  ngOnInit(): void {
  }

  onCancelClick(){
      this.dialogRef.close();
  }

  chooseVehicleType(vehicleType: string){
      this.vehicleType = vehicleType;
  }

  onAddClick(){
      this.dialogRef.close(this.vehicleType);
  }

  onClickedVehicleType(vehicleTypeDTO: TipoViaturaDTO){
      this.chooseVehicleType(vehicleTypeDTO.codigo);
  }

}
