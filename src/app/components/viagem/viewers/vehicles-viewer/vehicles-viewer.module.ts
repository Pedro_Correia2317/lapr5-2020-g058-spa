import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizacaoItemsModule } from 'src/app/components/utils/visualizacao-items/visualizacao-items.module';
import { RouterModule } from '@angular/router';
import { VehiclesViewerComponent } from './vehicles-viewer.component';

@NgModule({
  declarations: [VehiclesViewerComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [VehiclesViewerComponent, RouterModule]
})
export class VehiclesViewerModule { }
