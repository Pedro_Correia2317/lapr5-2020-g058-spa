import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesViewerComponent } from './vehicles-viewer.component';

describe('VehiclesViewerComponent', () => {
  let component: VehiclesViewerComponent;
  let fixture: ComponentFixture<VehiclesViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiclesViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
