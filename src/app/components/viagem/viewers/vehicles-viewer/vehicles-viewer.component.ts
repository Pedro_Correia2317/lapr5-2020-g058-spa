import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { Vehicle } from 'src/app/models/viagem/vehicle/Vehicle';
import { AccessVehiclesService } from 'src/app/services/viagem/AccessVehicles.service';

@Component({
  selector: 'app-vehicles-viewer',
  templateUrl: './vehicles-viewer.component.html',
  styleUrls: ['./vehicles-viewer.component.css']
})
export class VehiclesViewerComponent implements OnInit {

  sorts: string[] = ['INSERTION_DATE', 'LICENSE_PLATE', 'ENTRY_DATE', 'VIN'];

  @Output() clickedVehicle: EventEmitter<Vehicle> = new EventEmitter();

  vehicles: Vehicle[];

  pageCreationData: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  size: number;
  
  sort: string;
  
  isDescending: boolean;

  hasFinishedLoadingVehicles: boolean;

  constructor(private allVehicles: AccessVehiclesService) {
    this.vehicles = [];
  }

  ngOnInit() {
    this.sort = this.sorts[0];
  }

  onClickedVehicle(selectedVehicle: Vehicle){
    this.clickedVehicle.emit(selectedVehicle);
  }

  changePage(page: number){
    this.updateVehicles(page, this.size, this.sort, this.isDescending);
  }

  changeSize(size: number){
    this.size = size;
    this.updateVehicles(1, this.size, this.sort, this.isDescending);
  }

  changeSort(sort: string){
    this.sort = sort;
    this.updateVehicles(1, this.size, this.sort, this.isDescending);
  }

  changeOrder(isDescending: boolean){
    this.isDescending = isDescending;
    this.updateVehicles(1, this.size, this.sort, this.isDescending);
  }

  private updateVehicles(page: number, size: number, sort?: string, isDescending?: boolean){
    this.hasFinishedLoadingVehicles = false;
    this.allVehicles.searchVehicles(page, size, sort, isDescending).subscribe((results: QueryResultsDTO<Vehicle>) => {
      this.vehicles = results.results;
      this.pageCreationData = { numeroTotal: results.totalNumber, tamanhoPagina: size, pagina: page };
      this.hasFinishedLoadingVehicles = true;
    });
  }

}
