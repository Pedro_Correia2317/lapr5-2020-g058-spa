import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriversViewerComponent } from './drivers-viewer.component';
import { TranslateModule } from '@ngx-translate/core';
import { VisualizacaoItemsModule } from 'src/app/components/utils/visualizacao-items/visualizacao-items.module';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [DriversViewerComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [DriversViewerComponent, RouterModule]
})
export class DriversViewerModule { }
