import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriversViewerComponent } from './drivers-viewer.component';

describe('DriversViewerComponent', () => {
  let component: DriversViewerComponent;
  let fixture: ComponentFixture<DriversViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriversViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
