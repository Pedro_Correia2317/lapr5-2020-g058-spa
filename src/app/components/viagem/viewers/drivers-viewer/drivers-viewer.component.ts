import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Driver } from 'src/app/models/viagem/driver/Driver';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { AccessDriversService } from 'src/app/services/viagem/AccessDrivers.service';

@Component({
  selector: 'app-drivers-viewer',
  templateUrl: './drivers-viewer.component.html',
  styleUrls: ['./drivers-viewer.component.css']
})
export class DriversViewerComponent implements OnInit {

  sorts: string[] = ['INSERTION_DATE', 'NUMBER', 'NAME', 'BIRTH_DATE', 'START_CONTRACT_DATE', 'END_CONTRACT_DATE'];

  @Output() clickedDriver: EventEmitter<Driver> = new EventEmitter();

  drivers: Driver[];

  pageCreationData: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  size: number;
  
  sort: string;
  
  isDescending: boolean;

  search: string;

  hasFinishedLoadingDrivers: boolean;

  constructor(private allDrivers: AccessDriversService) {
    this.drivers = [];
  }

  ngOnInit() {
    this.sort = this.sorts[0];
  }

  onClickedDriver(selectedDriver: Driver){
    this.clickedDriver.emit(selectedDriver);
  }

  changePage(page: number){
    this.updateDrivers(page, this.size, this.sort, this.search, this.isDescending);
  }

  changeSearch(search: string){
    this.search = search;
    this.updateDrivers(1, this.size, this.sort, this.search, this.isDescending);
  }

  changeSize(size: number){
    this.size = size;
    this.updateDrivers(1, this.size, this.sort, this.search, this.isDescending);
  }

  changeSort(sort: string){
    this.sort = sort;
    this.updateDrivers(1, this.size, this.sort, this.search, this.isDescending);
  }

  changeOrder(isDescending: boolean){
    this.isDescending = isDescending;
    this.updateDrivers(1, this.size, this.sort, this.search, this.isDescending);
  }

  private updateDrivers(page: number, size: number, sort?: string, search?: string, isDescending?: boolean){
    this.hasFinishedLoadingDrivers = false;
    this.allDrivers.searchDrivers(page, size, sort, search, isDescending).subscribe((results: QueryResultsDTO<Driver>) => {
      this.drivers = results.results;
      this.pageCreationData = { numeroTotal: results.totalNumber, tamanhoPagina: size, pagina: page };
      this.hasFinishedLoadingDrivers = true;
    });
  }

}
