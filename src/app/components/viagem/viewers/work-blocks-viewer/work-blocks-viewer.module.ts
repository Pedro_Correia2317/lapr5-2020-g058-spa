
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';
import { WorkBlocksViewerComponent } from './work-blocks-viewer.component';

@NgModule({
  declarations: [WorkBlocksViewerComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [WorkBlocksViewerComponent, RouterModule]
})
export class WorkBlocksViewerModule { }