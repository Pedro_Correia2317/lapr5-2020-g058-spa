import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkBlocksViewerComponent } from './work-blocks-viewer.component';

describe('WorkBlocksViewerComponent', () => {
  let component: WorkBlocksViewerComponent;
  let fixture: ComponentFixture<WorkBlocksViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkBlocksViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkBlocksViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
