import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { WorkBlock } from 'src/app/models/viagem/workblocks/WorkBlock';
import { AccessWorkBlocksService } from 'src/app/services/viagem/AccesWorkBlock.service';

@Component({
  selector: 'app-work-blocks-viewer',
  templateUrl: './work-blocks-viewer.component.html',
  styleUrls: ['./work-blocks-viewer.component.css']
})
export class WorkBlocksViewerComponent implements OnInit {

  @Input() unbindedsOnly: boolean;

  @Output() clickedWorkBlock: EventEmitter<WorkBlock> = new EventEmitter();

  workBlocks: WorkBlock[];

  pageCreationData: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  hasFinishedLoadingWorkBlocks: boolean;

  constructor(private allWorkBlocks: AccessWorkBlocksService) {
    this.workBlocks = [];
    this.unbindedsOnly = true;
  }

  ngOnInit(): void {
  }

  onClickedWorkBlock(selectedWorkBlock: WorkBlock){
    this.clickedWorkBlock.emit(selectedWorkBlock);
  }

  changePage(page: number){
    this.updateWorkBlocks(page, this.tamanho, this.sort);
  }

  changeSize(size: number){
    this.tamanho = size;
    this.updateWorkBlocks(1, size, this.sort);
  }

  private updateWorkBlocks(pagina: number, tamanho: number, sort?: string){
    this.hasFinishedLoadingWorkBlocks = false;
    this.allWorkBlocks.searchWorkBlocks(pagina, tamanho, sort, this.unbindedsOnly).subscribe((results: QueryResultsDTO<WorkBlock>) => {
      this.workBlocks = results.results;
      this.pageCreationData = { numeroTotal: results.totalNumber, tamanhoPagina: tamanho, pagina: pagina };
      this.hasFinishedLoadingWorkBlocks = true;
    });
  }

}
