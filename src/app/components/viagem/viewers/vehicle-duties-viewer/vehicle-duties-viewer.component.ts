import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { VehicleDuty } from 'src/app/models/viagem/vehicleDuty/VehicleDuty';
import { AccessVehicleDutiesService } from 'src/app/services/viagem/AccessVehicleDuties.service';

@Component({
  selector: 'app-vehicle-duties-viewer',
  templateUrl: './vehicle-duties-viewer.component.html',
  styleUrls: ['./vehicle-duties-viewer.component.css']
})
export class VehicleDutiesViewerComponent implements OnInit {

  @Input() unbindedsOnly: boolean;

  sorts: string[] = ['INSERTION_DATE', 'CODE', 'START_TIME', 'END_TIME', 'NUMBER_OF_TRIPS'];

  @Output() clickedVehicleDuty: EventEmitter<VehicleDuty> = new EventEmitter();

  vehicleDuties: VehicleDuty[];

  pageCreationData: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  size: number;
  
  sort: string;
  
  isDescending: boolean;

  hasFinishedLoadingVehicleDuties: boolean;

  constructor(private allVehicleDuties: AccessVehicleDutiesService) {
    this.vehicleDuties = [];
    this.unbindedsOnly = false;
  }

  ngOnInit(): void {
    this.sort = this.sorts[0];
  }

  onClickedVehicleDuty(selectedVehicleDuty: VehicleDuty){
    this.clickedVehicleDuty.emit(selectedVehicleDuty);
  }

  changePage(page: number){
    this.updateVehicleDuties(page, this.size, this.sort, this.isDescending);
  }

  changeSize(size: number){
    this.size = size;
    this.updateVehicleDuties(1, size, this.sort, this.isDescending);
  }

  changeSort(sort: string){
    this.sort = sort;
    this.updateVehicleDuties(1, this.size, this.sort, this.isDescending);
  }

  changeOrder(isDescending: boolean){
    this.isDescending = isDescending;
    this.updateVehicleDuties(1, this.size, this.sort, this.isDescending);
  }

  private updateVehicleDuties(pagina: number, tamanho: number, sort?: string, isDescending?: boolean){
    this.hasFinishedLoadingVehicleDuties = false;
    this.allVehicleDuties.searchVehicleDuties(pagina, tamanho, sort, isDescending, this.unbindedsOnly).subscribe((results: QueryResultsDTO<VehicleDuty>) => {
      this.vehicleDuties = results.results;
      this.pageCreationData = { numeroTotal: results.totalNumber, tamanhoPagina: tamanho, pagina: pagina };
      this.hasFinishedLoadingVehicleDuties = true;
    });
  }

}
