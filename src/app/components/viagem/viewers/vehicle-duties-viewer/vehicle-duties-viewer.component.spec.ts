import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDutiesViewerComponent } from './vehicle-duties-viewer.component';

describe('VehicleDutiesViewerComponent', () => {
  let component: VehicleDutiesViewerComponent;
  let fixture: ComponentFixture<VehicleDutiesViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleDutiesViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDutiesViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
