
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';
import { VehicleDutiesViewerComponent } from './vehicle-duties-viewer.component';

@NgModule({
  declarations: [VehicleDutiesViewerComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [VehicleDutiesViewerComponent, RouterModule]
})
export class VehicleDutiesViewerModule { }