import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Trip } from 'src/app/models/viagem/trip/Trip';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { AccessTripsService } from 'src/app/services/viagem/AccessTrips.service';

@Component({
  selector: 'app-trips-viewers',
  templateUrl: './trips-viewers.component.html',
  styleUrls: ['./trips-viewers.component.css']
})
export class TripsViewersComponent implements OnInit {

  sorts: string[] = ['INSERTION_DATE', 'HOUR', 'LINE', 'PATH'];

  filters: string[] = ['NONE', '0h00m -> 2h00m', '2h00m -> 4h00m', '4h00m -> 6h00m', '6h00m -> 8h00m',
  '8h00m -> 10h00m', '10h00m -> 12h00m', '12h00m -> 14h00m', '14h00m -> 16h00m', '16h00m -> 18h00m', 
  '18h00m -> 20h00m', '20h00m -> 22h00m', '22h00m -> 24h00m'];

  @Input() unbindedsOnly: boolean;

  @Output() clickedTrip: EventEmitter<Trip> = new EventEmitter();

  trips: Trip[];

  pageCreationData: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  size: number;
  
  sort: string;
  
  isDescending: boolean;

  filter: string;

  hasFinishedLoadingTrips: boolean;

  constructor(private allTrips: AccessTripsService) {
    this.trips = [];
    this.unbindedsOnly = false;
  }

  ngOnInit(): void {
    this.filter = this.filters[0];
    this.sort = this.sorts[0];
  }

  onClickedTrip(selectedTrip: Trip){
    this.clickedTrip.emit(selectedTrip);
  }

  changePage(page: number){
    this.updateTrips(page, this.size, this.sort);
  }

  changeSize(size: number){
    this.size = size;
    this.updateTrips(1, size, this.sort);
  }

  changeSort(sort: string){
    this.sort = sort;
    this.updateTrips(1, this.size, this.sort, this.isDescending, this.filter);
  }

  changeFilter(filter: string){
    this.filter = filter;
    this.updateTrips(1, this.size, this.sort, this.isDescending, this.filter);
  }

  changeOrder(isDescending: boolean){
    this.isDescending = isDescending;
    this.updateTrips(1, this.size, this.sort, this.isDescending, this.filter);
  }

  private updateTrips(pagina: number, tamanho: number, sort?: string, isDescending?: boolean, filter?: string){
    this.hasFinishedLoadingTrips = false;
    this.allTrips.searchTrips(pagina, tamanho, sort, isDescending, filter, this.unbindedsOnly).subscribe((results: QueryResultsDTO<Trip>) => {
      this.trips = results.results;
      this.pageCreationData = { numeroTotal: results.totalNumber, tamanhoPagina: tamanho, pagina: pagina };
      this.hasFinishedLoadingTrips = true;
    });
  }

}
