
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';
import { TripsViewersComponent } from './trips-viewers.component';

@NgModule({
  declarations: [TripsViewersComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [TripsViewersComponent, RouterModule]
})
export class TripsViewerModule { }