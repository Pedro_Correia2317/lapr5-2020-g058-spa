import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsViewersComponent } from './trips-viewers.component';

describe('TripsViewersComponent', () => {
  let component: TripsViewersComponent;
  let fixture: ComponentFixture<TripsViewersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripsViewersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsViewersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
