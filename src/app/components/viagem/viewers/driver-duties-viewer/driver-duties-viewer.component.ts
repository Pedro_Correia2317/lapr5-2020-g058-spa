import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DriverDuty } from 'src/app/models/viagem/driverDuty/DriverDuty';
import { QueryResultsDTO } from 'src/app/models/viagem/utils/QueryResultsDTO';
import { AccessDriverDutiesService } from 'src/app/services/viagem/AccessDriverDuties.service';

@Component({
  selector: 'app-driver-duties-viewer',
  templateUrl: './driver-duties-viewer.component.html',
  styleUrls: ['./driver-duties-viewer.component.css']
})
export class DriverDutiesViewerComponent implements OnInit {

  @Output() clickedDriverDuty: EventEmitter<DriverDuty> = new EventEmitter();

  driverDuties: DriverDuty[];

  pageCreationData: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  hasFinishedLoadingDriverDuties: boolean;

  constructor(private allDriverDuties: AccessDriverDutiesService) {
    this.driverDuties = [];
  }

  ngOnInit(): void {
  }

  onClickedDriverDuty(selectedDriverDuty: DriverDuty){
    this.clickedDriverDuty.emit(selectedDriverDuty);
  }

  changePage(page: number){
    this.updateDriverDuties(page, this.tamanho, this.sort);
  }

  changeSize(size: number){
    this.tamanho = size;
    this.updateDriverDuties(1, size, this.sort);
  }

  private updateDriverDuties(pagina: number, tamanho: number, sort?: string){
    this.hasFinishedLoadingDriverDuties = false;
    this.allDriverDuties.searchDriverDuties(pagina, tamanho, sort).subscribe((results: QueryResultsDTO<DriverDuty>) => {
      this.driverDuties = results.results;
      this.pageCreationData = { numeroTotal: results.totalNumber, tamanhoPagina: tamanho, pagina: pagina };
      this.hasFinishedLoadingDriverDuties = true;
    });
  }

}
