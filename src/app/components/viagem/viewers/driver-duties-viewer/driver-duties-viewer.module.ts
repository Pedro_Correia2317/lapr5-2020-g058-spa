import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverDutiesViewerComponent } from './driver-duties-viewer.component';
import { VisualizacaoItemsModule } from 'src/app/components/utils/visualizacao-items/visualizacao-items.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [DriverDutiesViewerComponent],
  imports: [CommonModule, TranslateModule, VisualizacaoItemsModule],
  exports: [DriverDutiesViewerComponent, RouterModule]
})
export class DriverDutiesViewerModule { }
