import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverDutiesViewerComponent } from './driver-duties-viewer.component';

describe('DriverDutiesViewerComponent', () => {
  let component: DriverDutiesViewerComponent;
  let fixture: ComponentFixture<DriverDutiesViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverDutiesViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverDutiesViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
