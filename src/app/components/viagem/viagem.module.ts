import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViagemComponent } from './viagem.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '', component: ViagemComponent, children: 
      [
        {
          path: 'vehicles', 
          loadChildren: () => import('./items-display-pages/vehicles-displayer/vehicles-displayer.module').then(m => m.VehiclesDisplayerModule)
        },
        {
          path: 'newVehicle', 
          loadChildren: () => import('./items-creation-pages/vehicles-creation/vehicles-creation.module').then(m => m.VehiclesCreationModule)
        },
        {
          path: 'drivers', 
          loadChildren: () => import('./items-display-pages/drivers-displayer/drivers-displayer.module').then(m => m.DriversDisplayerModule)
        },
        {
          path: 'newDriver', 
          loadChildren: () => import('./items-creation-pages/drivers-creation/drivers-creation.module').then(m => m.DriversCreationModule)
        },
        {
          path: 'trips', 
          loadChildren: () => import('./items-display-pages/trips-displayer/trips-displayer.module').then(m => m.TripsDisplayerModule)
        },
        {
          path: 'newTrip', 
          loadChildren: () => import('./items-creation-pages/trips-creation/trips-creation.module').then(m => m.TripsCreationModule)
        },
        {
          path: 'vehicleDuties', 
          loadChildren: () => import('./items-display-pages/vehicle-duties-displayer/vehicle-duties-displayer.module').then(m => m.VehicleDutiesDisplayerModule)
        },
        {
          path: 'newVehicleDuty', 
          loadChildren: () => import('./items-creation-pages/vehicle-duties-creation/vehicle-duties-creation.module').then(m => m.VehicleDutiesCreationModule)
        },
        {
          path: 'workBlocks', 
          loadChildren: () => import('./items-display-pages/work-blocks-displayer/work-blocks-displayer.module').then(m => m.WorkBlocksDisplayerModule)
        },
        {
          path: 'newWorkBlock', 
          loadChildren: () => import('./items-creation-pages/work-blocks-creation/work-blocks-creation.module').then(m => m.WorkBlocksCreationModule)
        },
        {
          path: 'driverDuties', 
          loadChildren: () => import('./items-display-pages/driver-duties-displayer/driver-duties-displayer.module').then(m => m.DriverDutiesDisplayerModule)
        },
        {
          path: 'newDriverDuty', 
          loadChildren: () => import('./items-creation-pages/driver-duties-creation/driver-duties-creation.module').then(m => m.DriverDutiesCreationModule)
        },
        {
          path: 'importation', 
          loadChildren: () => import('./items-creation-pages/importation/importation.module').then(m => m.ImportationModule)
        }
      ]
  }
];

@NgModule({
  declarations: [ViagemComponent],
  imports: [CommonModule, TranslateModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViagemModule { }
