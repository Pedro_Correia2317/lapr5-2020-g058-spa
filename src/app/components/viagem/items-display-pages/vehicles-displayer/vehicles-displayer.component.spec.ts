import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesDisplayerComponent } from './vehicles-displayer.component';

describe('VehiclesDisplayerComponent', () => {
  let component: VehiclesDisplayerComponent;
  let fixture: ComponentFixture<VehiclesDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiclesDisplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
