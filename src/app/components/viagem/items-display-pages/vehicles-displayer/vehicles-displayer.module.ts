import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehiclesDisplayerComponent } from './vehicles-displayer.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { VehiclesViewerModule } from '../../viewers/vehicles-viewer/vehicles-viewer.module';

const routes: Routes = [
  {path: '', component: VehiclesDisplayerComponent}
];

@NgModule({
declarations: [VehiclesDisplayerComponent],
imports: [VehiclesViewerModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class VehiclesDisplayerModule { }
