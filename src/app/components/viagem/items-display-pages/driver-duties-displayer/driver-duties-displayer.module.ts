
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { DriverDutiesDisplayerComponent } from './driver-duties-displayer.component';
import { DriverDutiesViewerModule } from '../../viewers/driver-duties-viewer/driver-duties-viewer.module';

const routes: Routes = [
  {path: '', component: DriverDutiesDisplayerComponent}
];

@NgModule({
declarations: [DriverDutiesDisplayerComponent],
imports: [DriverDutiesViewerModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class DriverDutiesDisplayerModule { }
