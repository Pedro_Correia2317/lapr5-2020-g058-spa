import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverDutiesDisplayerComponent } from './driver-duties-displayer.component';

describe('DriverDutiesDisplayerComponent', () => {
  let component: DriverDutiesDisplayerComponent;
  let fixture: ComponentFixture<DriverDutiesDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverDutiesDisplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverDutiesDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
