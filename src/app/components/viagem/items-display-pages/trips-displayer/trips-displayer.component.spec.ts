import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TripsDisplayerComponent } from './trips-displayer.component';

describe('TripsDisplayerComponent', () => {
  let component: TripsDisplayerComponent;
  let fixture: ComponentFixture<TripsDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripsDisplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripsDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
