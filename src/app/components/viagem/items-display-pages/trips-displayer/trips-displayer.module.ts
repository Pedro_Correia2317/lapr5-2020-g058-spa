import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TripsDisplayerComponent } from './trips-displayer.component';
import { TripsViewerModule } from '../../viewers/trips-viewers/trips-viewers.module';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {path: '', component: TripsDisplayerComponent}
];

@NgModule({
declarations: [TripsDisplayerComponent],
imports: [TripsViewerModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class TripsDisplayerModule { }
