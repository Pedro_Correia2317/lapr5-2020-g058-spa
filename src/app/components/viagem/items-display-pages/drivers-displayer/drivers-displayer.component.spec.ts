import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriversDisplayerComponent } from './drivers-displayer.component';

describe('DriversDisplayerComponent', () => {
  let component: DriversDisplayerComponent;
  let fixture: ComponentFixture<DriversDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriversDisplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
