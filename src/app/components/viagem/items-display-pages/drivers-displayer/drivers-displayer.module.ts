import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriversDisplayerComponent } from './drivers-displayer.component';
import { TranslateModule } from '@ngx-translate/core';
import { DriversViewerModule } from '../../viewers/drivers-viewer/drivers-viewer.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: DriversDisplayerComponent}
];

@NgModule({
declarations: [DriversDisplayerComponent],
imports: [DriversViewerModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class DriversDisplayerModule { }
