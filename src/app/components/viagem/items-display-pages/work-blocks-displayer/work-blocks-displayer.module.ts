import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkBlocksDisplayerComponent } from './work-blocks-displayer.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { WorkBlocksViewerModule } from '../../viewers/work-blocks-viewer/work-blocks-viewer.module';

const routes: Routes = [
  {path: '', component: WorkBlocksDisplayerComponent}
];

@NgModule({
declarations: [WorkBlocksDisplayerComponent],
imports: [WorkBlocksViewerModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class WorkBlocksDisplayerModule { }
