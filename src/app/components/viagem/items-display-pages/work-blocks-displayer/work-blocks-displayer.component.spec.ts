import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkBlocksDisplayerComponent } from './work-blocks-displayer.component';

describe('WorkBlocksDisplayerComponent', () => {
  let component: WorkBlocksDisplayerComponent;
  let fixture: ComponentFixture<WorkBlocksDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkBlocksDisplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkBlocksDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
