import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleDutiesDisplayerComponent } from './vehicle-duties-displayer.component';

describe('VehicleDutiesDisplayerComponent', () => {
  let component: VehicleDutiesDisplayerComponent;
  let fixture: ComponentFixture<VehicleDutiesDisplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleDutiesDisplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleDutiesDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
