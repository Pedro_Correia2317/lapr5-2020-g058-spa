
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehicleDutiesDisplayerComponent } from './vehicle-duties-displayer.component';
import { RouterModule, Routes } from '@angular/router';
import { VehicleDutiesViewerModule } from '../../viewers/vehicle-duties-viewer/vehicle-duties-viewer.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {path: '', component: VehicleDutiesDisplayerComponent}
];

@NgModule({
declarations: [VehicleDutiesDisplayerComponent],
imports: [VehicleDutiesViewerModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class VehicleDutiesDisplayerModule { }
