import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PrivacyPolicyPageComponent } from './privacy-policy-page/privacy-policy-page.component';
import { TermsAndConditionsPageComponent } from './terms-and-conditions-page/terms-and-conditions-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { AccountPageComponent } from './account-page/account-page.component';

const routes: Route[] = [
  { path: '', component: MainPageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'account', component: AccountPageComponent },
  { path: 'signup', component: SignupPageComponent },
  { path: 'privacy', component: PrivacyPolicyPageComponent },
  { path: 'terms', component: TermsAndConditionsPageComponent }
]

@NgModule({
  declarations: [LoginPageComponent, SignupPageComponent, PrivacyPolicyPageComponent, TermsAndConditionsPageComponent, MainPageComponent, AccountPageComponent],
  imports: [
    TranslateModule, FormsModule, ReactiveFormsModule, CommonModule, RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LoginModule { }
