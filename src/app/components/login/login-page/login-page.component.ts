import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoginRequestDTO } from 'src/app/models/login/LoginRequestDTO';
import { AuthService } from 'src/app/services/login/Auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  loginForm: FormGroup;

  errorLoginMessages: string[];

  isLoggingIn: boolean;

  constructor(private builder: FormBuilder, private auth: AuthService) {
    this.loginForm = this.builder.group({
      username: '',
      password: ''
    });
    this.errorLoginMessages = [];
    this.isLoggingIn = false;
  }

  ngOnInit(): void {
  }

  async onSubmit(){
    this.isLoggingIn = true;
    const dto: LoginRequestDTO = new LoginRequestDTO();
    dto.username = this.loginForm.value.username;
    dto.password = this.loginForm.value.password;
    this.auth.login(dto).subscribe(
      user => this.isLoggingIn = false,
      error => { 
        this.errorLoginMessages = error.error;
        this.isLoggingIn = false; 
      }
      );
  }

}
