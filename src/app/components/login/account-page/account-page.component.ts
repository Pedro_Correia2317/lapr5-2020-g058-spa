import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { UpdateUserRequestDTO } from 'src/app/models/login/UpdateUserRequestDTO';
import { AcessUsersService } from 'src/app/services/login/AccessUsers.service';
import { AuthService } from 'src/app/services/login/Auth.service';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.css']
})
export class AccountPageComponent implements OnInit {

  updateAccountForm : FormGroup;

  errorSignupMessages: string[];

  username: string;

  checkboxValue: boolean;

  constructor(private formBuilder: FormBuilder, 
              private allUsers: AcessUsersService, 
              private translator: TranslateService,
              private auth: AuthService)
  {
    this.updateAccountForm = this.formBuilder.group({
      newUsername: '',
      newBirthDate: ''
    });
    this.errorSignupMessages = [];
    this.username = this.auth.getUser();
    this.checkboxValue = false;
  }

  ngOnInit(): void {
  }

  onUpdateSubmit(): void {
    this.errorSignupMessages = [];
    const dto = new UpdateUserRequestDTO();
    dto.newUsername = this.updateAccountForm.value.newUsername;
    dto.newBirthDate = this.updateAccountForm.value.newBirthDate;
    this.allUsers.updateUser(dto, this.username).subscribe(
      success => this.errorSignupMessages = [this.translator.instant("accountPage.createSuccess")], 
      errorMessages => this.errorSignupMessages = errorMessages.error
    );
  }

  onDeleteSubmit(): void {
    this.errorSignupMessages = [];
    let success = true;
    if(this.checkboxValue == false){
      this.errorSignupMessages.push("accountPage.notConfirmingDeletion");
      success = false;
    }
    if(success){
      this.allUsers.deleteUser(this.username).subscribe(
        success => this.auth.logout('/'), 
        errorMessages => this.errorSignupMessages = errorMessages.error
      );
    }
  }

}
