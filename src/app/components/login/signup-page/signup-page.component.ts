import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Configurations } from 'src/app/config/Configurations';
import { CreateUserRequestDTO } from 'src/app/models/login/CreateUserRequestDTO';
import { AcessUsersService } from 'src/app/services/login/AccessUsers.service';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.css']
})
export class SignupPageComponent implements OnInit {

  signupForm : FormGroup;

  errorSignupMessages: string[];

  isSigningIn: boolean;

  constructor(private formBuilder: FormBuilder, private allUsers: AcessUsersService, private translator: TranslateService)
  {
    this.signupForm = this.formBuilder.group({
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
      birthDate: '',
      agreeTerms: false
    });
    this.errorSignupMessages = [];
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.errorSignupMessages = [];
    let success1 = true, success2 = true;
    if (this.signupForm.value.password != this.signupForm.value.confirmPassword){
      this.errorSignupMessages.push("signupPage.unmatchingPasswords");
      success1 = false;
    }
    if(this.signupForm.value.agreeTerms == false){
      this.errorSignupMessages.push("signupPage.notAgreeingTerms");
      success2 = false;
    }
    if(success1 && success2){
      this.isSigningIn = true;
      const dto = new CreateUserRequestDTO();
      dto.username = this.signupForm.value.username;
      dto.email = this.signupForm.value.email;
      dto.password = this.signupForm.value.password;
      dto.role = Configurations.CLIENT;
      const aux: string[] = this.signupForm.value.birthDate.split("-");
      dto.birthdate = aux[2] + "-" + aux[1] + "-" + aux[0];
      this.allUsers.sendUser(dto).subscribe(
        user => {
          alert(this.translator.instant("signupPage.createSuccess"));
          this.isSigningIn = false;
        }, 
        errorMessages => {
          this.errorSignupMessages = errorMessages.error;
          this.isSigningIn = false;
        }
      );
    }
  }

}
