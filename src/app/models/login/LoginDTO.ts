
export class LoginDTO {

    public username: string;

    public token: string;
    
    public role: string;
}