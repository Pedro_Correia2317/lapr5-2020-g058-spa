
export class UserDTO {

    public username: string;

    public email: string;

    public birthDate: string;

    public role: string;

}