
export class CreateUserRequestDTO {

    public username: string;

    public email: string;

    public password: string;

    public birthdate: string;

    public role: string;
}