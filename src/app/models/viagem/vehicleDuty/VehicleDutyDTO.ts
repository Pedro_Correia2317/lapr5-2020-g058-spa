
export class VehicleDutyDTO {

    public code: string;

    public trips: VehicleDutyTripDTO[];
    
}

export class VehicleDutyTripDTO {

    public tripCode: string;

    public startTimeSeconds: number;

    public endTimeSeconds: number;

}