
export class VehicleDuty {

    public code: string;

    public listTrips: VehicleDutyTrip[];

    public startTime: string;

    public endTime: string;
}

export class VehicleDutyTrip {

    public tripCode: string;

    public startTime: string;

    public endTime: string;
}