
export class PlanningDriverDutyDTO {

    public number: number;

    public list_work_blocks: PlanningWorkBlockDTO[]

}

export class PlanningWorkBlockDTO {

    public code: string;
    
}