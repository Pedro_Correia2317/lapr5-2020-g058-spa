
export class TripDTO {

    public tripCode: string;

    public lineCode: string;

    public pathCode: string;

    public starHour: string;
    
}