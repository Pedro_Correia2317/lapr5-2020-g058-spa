
export class CreateTripRequestDTO {

    public code: string;

    public line: string;

    public path: string;

    public startHour: string;
    
}