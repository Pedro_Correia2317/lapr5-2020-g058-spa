
export class Trip {

    public code: string;

    public line: string;

    public path: string;

    public startHour: string;

    public passingTimes: PassingTime[];
}

export class PassingTime {

    public node: string;

    public time: string;
}