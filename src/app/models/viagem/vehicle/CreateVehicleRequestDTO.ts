
export class CreateVehicleRequestDTO {

    public licensePlate: string;

    public vin: string;

    public entryDate: string;

    public vehicleType: string;
    
}