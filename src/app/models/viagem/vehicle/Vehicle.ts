
export class Vehicle {

    public licensePlate: string;

    public vin: string;

    public entryDate: string;

    public vehicleType: string;
}