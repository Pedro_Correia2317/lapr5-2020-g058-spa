
export class VehicleDTO {

    public licensePlate: string;

    public vin: string;

    public entryDate: string;

    public typeCode: string;
    
}