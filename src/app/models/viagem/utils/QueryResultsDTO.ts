
export class QueryResultsDTO<T> {

    public totalNumber: number;

    public results: T[];
    
}