
export class CreateWorkBlockRequestDTO {

    public codes: string[];

    public vehicleDuties: string[];

    public maxBlockDuration: number;

    public numberMaxBlocks: number;
        
}