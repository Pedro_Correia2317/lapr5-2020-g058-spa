
export class WorkBlockDTO {

    public code: string;

    public trips: WorkBlockTripDTO[];
    
}

export class WorkBlockTripDTO {

    public dutyCode: string;

    public durationSeconds: number;

}