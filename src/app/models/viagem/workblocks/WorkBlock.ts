
export class WorkBlock {

    public code: string;

    public listTrips: WorkBlockTrip[];
}

export class WorkBlockTrip {

    public dutyCode: string;

    public durationSeconds: number;
}