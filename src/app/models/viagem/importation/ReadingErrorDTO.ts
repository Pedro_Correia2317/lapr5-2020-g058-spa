
export class ReadingErrorDTO {

    public description: string;

    public line: number;

    public charIndex: number;

}