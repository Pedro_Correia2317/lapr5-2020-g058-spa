
import { CreateDriverDutyRequestDTO } from "../driverDuty/CreateDriverDutyRequestDTO";
import { CreateTripRequestDTO } from "../trip/CreateTripRequestDTO";
import { CreateVehicleDutyRequestDTO } from "../vehicleDuty/CreateVehicleDutyRequestDTO";
import { CreateWorkBlockRequestDTO } from "../workblocks/CreateWorkBlockRequestDTO";
import { ReadingErrorDTO } from "./ReadingErrorDTO";

export class ImportationDetailsDTO {

    public errors: ReadingErrorDTO[];

    public trips: CreateTripRequestDTO[];

    public vehicleDuties: CreateVehicleDutyRequestDTO[];

    public workBlocks: CreateWorkBlockRequestDTO[];

    public driverDuties: CreateDriverDutyRequestDTO[];

}