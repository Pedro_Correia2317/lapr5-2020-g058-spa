
export class Driver {

    public mecanographicNumber: string;

    public name: string;

    public birthDate: string;

    public ccNumber: string;

    public nif: string;

    public initialContractDate: string;

    public endContractDate: string;

    public driverTypeCodes: string[];
}