
export class CreateDriverRequestDTO {

    public mecNumber: string;

    public driverName: string;

    public birthDate: string;

    public cCNumb: string;

    public nIF: string;

    public initDate: string;

    public endDate: string;

    public listDriverTypeCode: string[];
    
}