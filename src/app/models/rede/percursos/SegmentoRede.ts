
export class SegmentoRede {

    public abreviaturaNoInicio: string;

    public abreviaturaNoFim: string;

    public distancia: number;

    public sequencia: number;

    public duracao: number;

}