
export class PercursoDTO {

    public codigo: string;

    public descricao: string;

    public distanciaTotal: number;

    public duracaoTotal: number;

}