import { SegmentoRede } from "./SegmentoRede";

export class Percurso {

    public codigo: string;

    public descricao: string;

    public distanciaTotal: number;

    public duracaoTotal: number;

    public segmentos: SegmentoRede[];

}