import { SegmentoRedeDTO } from "./SegmentoRedeDTO";

export class PercursoCompletoDTO {

    public codigo: string;

    public descricao: string;

    public distanciaTotal: number;

    public duracaoTotal: number;

    public segmentos: SegmentoRedeDTO[];

}