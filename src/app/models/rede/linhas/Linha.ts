
export class Linha {
    
    public codigo: string;

    public nome: string;

    public percursosIda: string[];

    public percursosVolta: string[];

    public percursosReforco: string[];

    public percursosVazios: string[];

    public tiposViaturas: string[];

    public restricaoTiposViaturas: string;

    public tiposTripulantes: string[];

    public restricaoTiposTripulantes: string;

}