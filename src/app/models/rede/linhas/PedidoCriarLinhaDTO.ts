
export class PedidoCriarLinhaDTO {

    public codigo: string;

    public nome: string;

    public percursosIda: string[];

    public percursosVolta: string[];

    public percursosReforco: string[];

    public percursosVazio: string[];

    public tiposViaturas: string[];

    public tiposTripulantes: string[];

    public tipoNecessidadeTipoViatura: string;

    public tipoNecessidadeTipoTripulante: string;

}