
export class NoDTO {

    public abreviatura: string;

    public nome: string;

    public tipoNo: string;

    public latitude: number;

    public longitude: number;

    public tempos: TempoDeslocacaoDTO[];

    public capacidade: number;

    constructor(){
        this.tempos = [];
    }
    
}

export class TempoDeslocacaoDTO {

    public tempo: number;

    public abreviaturaNo: string;

}