import { TipoCombustivel } from "./TipoCombustivel";

export class TipoViatura {

    public codigo: string;

    public descricao: string;

    public autonomia: number;

    public velocidadeMedia: number;

    public custo: number;

    public consumo: number;

    public emissoes: number;

    public tipoCombustivel: TipoCombustivel;

}