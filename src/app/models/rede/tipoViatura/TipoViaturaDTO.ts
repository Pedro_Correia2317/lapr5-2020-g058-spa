import { TipoCombustivelDTO } from "./TipoCombustivelDTO";


export class TipoViaturaDTO {

    public codigo: string;

    public descricao: string;

    public autonomia: number;

    public velocidadeMedia: number;

    public custo: number;

    public consumo: number;

    public emissoes: number;

    public tipoCombustivel: TipoCombustivelDTO;

}