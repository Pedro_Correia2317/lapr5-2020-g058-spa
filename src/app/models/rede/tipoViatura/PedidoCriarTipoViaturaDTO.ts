

export class PedidoCriarTipoViaturaDTO {

    public codigo: string;

    public descricao: string;
    
    public autonomia: number;

    public custo: number;
    
    public consumo: number;

    public velocidadeMedia: number;
    
    public emissoes: number;

    public tipoCombustivel: string;
    
}