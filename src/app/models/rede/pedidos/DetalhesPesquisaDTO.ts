
export class DetalhesPesquisaDTO<T> {

    public sucesso: boolean;

    public descricao: string;

    public lista: T[];

    public numeroTotal: number;
    
}